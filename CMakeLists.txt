cmake_minimum_required(VERSION 3.11)
project(lmn_smgw_simulator VERSION 0.0.1 LANGUAGES CXX)

option(SMGW_STATIC_BUILD "" OFF)


# Dependencies
include(cmake/dependencies.cmake)


# Warnings
if(MSVC)
	string(REGEX REPLACE "/W3" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
	add_compile_options(/W4 /permissive- /w14640)
else()
	add_compile_options(-Wall -Wextra -Wnon-virtual-dtor -pedantic)
endif()


# Library
add_library(lmn_wired
	src/cosem.cpp
	src/error.cpp
	src/hdlc/crc.h
	src/hdlc/hdlc.cpp
	src/hdlc/lmn/master.cpp
	src/hdlc/lmn/utility.cpp
    src/utility/openssl_helper.cpp
    src/utility/openssl_helper.h
	src/lmn_interface.cpp
	src/log.cpp
    src/meter_profile.cpp
	src/serial_port.cpp
	src/sml.cpp
    src/sym.cpp
	src/tls.cpp

	include/smgw/hdlc/address.h
	include/smgw/hdlc/control_field.h
	include/smgw/hdlc/frame.h
	include/smgw/hdlc/lmn/address.h
	include/smgw/hdlc/lmn/master.h
	include/smgw/hdlc/lmn/utility.h
	include/smgw/serial/config.h
	include/smgw/serial/serial_interface.h
	include/smgw/serial/serial_port.h
	include/smgw/utility/transceive.h
	include/smgw/utility/utility.h
	include/smgw/config.h
	include/smgw/cosem.h
	include/smgw/error.h
	include/smgw/hdlc.h
	include/smgw/lmn_interface.h
	include/smgw/log.h
	include/smgw/meter_profile.h
	include/smgw/obis.h
	include/smgw/serial.h
	include/smgw/sml.h
	include/smgw/sym.h
	include/smgw/tls.h
)
target_include_directories(lmn_wired PUBLIC include)

target_compile_features(lmn_wired PUBLIC cxx_std_17)
set_property(TARGET lmn_wired PROPERTY CXX_EXTENSIONS OFF)

target_link_libraries(lmn_wired PUBLIC
	Boost::system
	spdlog::spdlog
	smlpp::smlpp
	OpenSSL::SSL
	OpenSSL::Crypto
	Threads::Threads
)

if(MSVC)
    target_link_libraries(lmn_wired PUBLIC wsock32 Boost::regex Boost::date_time)
    target_compile_definitions(lmn_wired PUBLIC
        BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE
        _SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING
    )
    if(SMGW_STATIC_BUILD)
        target_link_libraries(lmn_wired PUBLIC legacy_stdio_definitions crypt32)
    endif()
elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 9)
        target_link_libraries(lmn_wired PUBLIC stdc++fs)
    endif()
    if(SMGW_STATIC_BUILD)
        target_link_libraries(lmn_wired PUBLIC dl)
    endif()
endif()




# Application
add_executable(${PROJECT_NAME}
	src/app/config.cpp
    src/app/config.h
	src/app/http_server.cpp
	src/app/http_server.h
	src/app/main.cpp
	src/app/request_handler.cpp
	src/app/request_handler.h
)
target_link_libraries(${PROJECT_NAME} PUBLIC
	lmn_wired
	args
	yaml-cpp
	nlohmann_json::nlohmann_json
)

if(MSVC)
    target_compile_definitions(${PROJECT_NAME} PUBLIC _CRT_SECURE_NO_WARNINGS)
endif()

# link time optimization
include(CheckIPOSupported)
check_ipo_supported(RESULT ipo_supported)
if(ipo_supported AND CMAKE_BUILD_TYPE MATCHES ".*Rel.*")
	message(STATUS "Enabling link time optimization.")
	set_property(TARGET ${PROJECT_NAME} lmn_wired PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()



# Tests
enable_testing()
add_subdirectory(test)
