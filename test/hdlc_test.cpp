#include "test_config.h"

#include <smgw/hdlc.h>
#include <smgw/hdlc/lmn/utility.h>
#include <smgw/serial/serial_interface.h>
#include <smgw/utility/utility.h>

using namespace smgw;

TEST_CASE("hdlc address parsing")
{
	SUBCASE("valid address bytes for broadcast messages")
	{
		auto a = hdlc::deserialize_address(0xFE, 0x02); // broadcast address + query new

		CHECK(a.dynamic_address == hdlc::lmn::address::broadcast);
		CHECK(a.session_protocol == hdlc::lmn::protocol_selector::query_new);
	}

	SUBCASE("valid address bytes for sml_cosem messages")
	{
		auto a = hdlc::deserialize_address(0xAA, 0x06); // address 0x55 + sml_cosem

		CHECK(static_cast<u8>(a.dynamic_address) == 0x55);
		CHECK(a.session_protocol == hdlc::lmn::protocol_selector::sml_cosem);
	}
}

TEST_CASE("hdlc control field format parsing")
{
	u8 i_byte   = 0b101'1'101'0;
	u8 rr_byte  = 0b101'1'0001;
	u8 rnr_byte = 0b101'1'0101;
	u8 ui_byte  = 0b000'1'0011;

	SUBCASE("i frame shows correct ssn and rsn")
	{
		hdlc::frame_function::i i_format{i_byte};
		CHECK(u8(i_format.send_sequence_number) == 0b101);
		CHECK(u8(i_format.receive_sequence_number) == 0b101);
	}

	SUBCASE("rr frame shows correct rsn")
	{
		hdlc::frame_function::rr rr_format{rr_byte};
		CHECK(u8(rr_format.receive_sequence_number) == 0b101);
	}

	SUBCASE("rnr frame shows correct rsn")
	{
		hdlc::frame_function::rnr rnr_format{rnr_byte};
		CHECK(u8(rnr_format.receive_sequence_number) == 0b101);
	}

	SUBCASE("automatic parsing works")
	{
		auto control = hdlc::deserialize_control_field(i_byte);
		CHECK(std::holds_alternative<hdlc::frame_function::i>(control));

		control = hdlc::deserialize_control_field(rnr_byte);
		CHECK(std::holds_alternative<hdlc::frame_function::rnr>(control));

		control = hdlc::deserialize_control_field(ui_byte);
		CHECK(std::holds_alternative<hdlc::frame_function::ui>(control));
	}

	SUBCASE("deserializer throws on invalid values")
	{
		u8 invalid = 0b0000'1101;

		CHECK_THROWS_AS(hdlc::deserialize_control_field(invalid), std::system_error);
	}
}

TEST_CASE("hdlc frame parsing")
{
	SUBCASE("parsing an empty broadcast frame")
	{
		vec<u8> broadcast_frame{0x7E, 0xA0, 0x09, 0xFE, 0x03, 0x02, 0x03, 0x13, 0x84, 0x2A, 0x7E};

		auto f = hdlc::deserialize_frame(broadcast_frame);

		CHECK(f.destination.dynamic_address == hdlc::lmn::address::broadcast);
		CHECK(f.destination.session_protocol == hdlc::lmn::protocol_selector::query_new);
		CHECK(f.source.dynamic_address == hdlc::lmn::address::smgw);
		CHECK(f.source.session_protocol == hdlc::lmn::protocol_selector::query_new);
		CHECK(std::holds_alternative<hdlc::frame_function::ui>(f.control));
		CHECK(f.payload.empty());
	}

	SUBCASE("parsing an information frame with dummy payload")
	{
		vec<u8> i_frame{
		    0x7E, 0xA0, 0xB3, 0x02, 0x07, 0x74, 0x07, 0x30, 0x7A, 0x79, 0x1B, 0x1B, 0x1B, 0x1B,
		    0x01, 0x01, 0x01, 0x01, 0x76, 0x02, 0x35, 0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x01,
		    0x01, 0x76, 0x01, 0x0B, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
		    0x02, 0x30, 0x0B, 0x0A, 0x01, 0x45, 0x4D, 0x48, 0x00, 0x00, 0x4A, 0xF1, 0xFD, 0x72,
		    0x62, 0x01, 0x64, 0x1C, 0xB0, 0xA8, 0x01, 0x63, 0x9F, 0x2E, 0x00, 0x76, 0x02, 0x36,
		    0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x05, 0x01, 0x73, 0x0B, 0x0A, 0x01, 0x45, 0x4D,
		    0x48, 0x00, 0x00, 0x4A, 0xF1, 0xFD, 0x73, 0x0A, 0x01, 0x00, 0x01, 0x08, 0x00, 0xFF,
		    0x80, 0x02, 0x00, 0x03, 0x00, 0x01, 0x03, 0x00, 0x02, 0x73, 0x0A, 0x01, 0x00, 0x01,
		    0x08, 0x00, 0xFF, 0x80, 0x02, 0x00, 0x01, 0x72, 0x73, 0x03, 0x00, 0x01, 0x72, 0x62,
		    0x01, 0x07, 0x01, 0x00, 0x01, 0x08, 0x00, 0xFF, 0x01, 0x73, 0x03, 0x00, 0x02, 0x72,
		    0x62, 0x01, 0x64, 0x2A, 0x3F, 0x4E, 0x01, 0x63, 0xAE, 0xAF, 0x00, 0x76, 0x02, 0x37,
		    0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x02, 0x01, 0x71, 0x01, 0x63, 0x4F, 0xF8, 0x00,
		    0x00, 0x00, 0x1B, 0x1B, 0x1B, 0x1B, 0x1A, 0x02, 0xB2, 0xEB, 0x0A, 0x9A, 0x7E};

		vec<u8> payload{i_frame.begin() + 10, i_frame.end() - 3};

		auto f = hdlc::deserialize_frame(i_frame);

		CHECK(f.destination.dynamic_address == hdlc::lmn::address::smgw);
		CHECK(f.destination.session_protocol == hdlc::lmn::protocol_selector::sml_cosem);
		CHECK(f.source.dynamic_address == hdlc::lmn::address(0x3A));
		CHECK(f.source.session_protocol == hdlc::lmn::protocol_selector::sml_cosem);
		REQUIRE(std::holds_alternative<hdlc::frame_function::i>(f.control));
		CHECK(u8(std::get<hdlc::frame_function::i>(f.control).receive_sequence_number) == 1);
		CHECK(u8(std::get<hdlc::frame_function::i>(f.control).send_sequence_number) == 0);
		CHECK(f.payload == payload);
	}
}

TEST_CASE("hdlc frame writing")
{
	using namespace hdlc;
	SUBCASE("writing a broadcast frame")
	{
		vec<u8> real_broadcast_frame{0x7E, 0xA0, 0x09, 0xFE, 0x03, 0x02,
		                             0x03, 0x13, 0x84, 0x2A, 0x7E};

		auto destination = address{lmn::address::broadcast, lmn::protocol_selector::query_new};
		auto source      = address{lmn::address::smgw, lmn::protocol_selector::query_new};
		auto control     = control_field{frame_function::ui{}};
		auto broadcast   = frame{destination, source, control, {}};

		auto bytes = broadcast.serialize();

		CHECK(bytes == real_broadcast_frame);
	}

	SUBCASE("writing an information frame")
	{
		vec<u8> real_i_frame{
		    0x7E, 0xA0, 0xB3, 0x02, 0x07, 0x74, 0x07, 0x30, 0x7A, 0x79, 0x1B, 0x1B, 0x1B, 0x1B,
		    0x01, 0x01, 0x01, 0x01, 0x76, 0x02, 0x35, 0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x01,
		    0x01, 0x76, 0x01, 0x0B, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
		    0x02, 0x30, 0x0B, 0x0A, 0x01, 0x45, 0x4D, 0x48, 0x00, 0x00, 0x4A, 0xF1, 0xFD, 0x72,
		    0x62, 0x01, 0x64, 0x1C, 0xB0, 0xA8, 0x01, 0x63, 0x9F, 0x2E, 0x00, 0x76, 0x02, 0x36,
		    0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x05, 0x01, 0x73, 0x0B, 0x0A, 0x01, 0x45, 0x4D,
		    0x48, 0x00, 0x00, 0x4A, 0xF1, 0xFD, 0x73, 0x0A, 0x01, 0x00, 0x01, 0x08, 0x00, 0xFF,
		    0x80, 0x02, 0x00, 0x03, 0x00, 0x01, 0x03, 0x00, 0x02, 0x73, 0x0A, 0x01, 0x00, 0x01,
		    0x08, 0x00, 0xFF, 0x80, 0x02, 0x00, 0x01, 0x72, 0x73, 0x03, 0x00, 0x01, 0x72, 0x62,
		    0x01, 0x07, 0x01, 0x00, 0x01, 0x08, 0x00, 0xFF, 0x01, 0x73, 0x03, 0x00, 0x02, 0x72,
		    0x62, 0x01, 0x64, 0x2A, 0x3F, 0x4E, 0x01, 0x63, 0xAE, 0xAF, 0x00, 0x76, 0x02, 0x37,
		    0x62, 0x00, 0x62, 0x00, 0x72, 0x63, 0x02, 0x01, 0x71, 0x01, 0x63, 0x4F, 0xF8, 0x00,
		    0x00, 0x00, 0x1B, 0x1B, 0x1B, 0x1B, 0x1A, 0x02, 0xB2, 0xEB, 0x0A, 0x9A, 0x7E};

		vec<u8> payload{real_i_frame.begin() + 10, real_i_frame.end() - 3};

		auto destination = address{lmn::address::smgw, lmn::protocol_selector::sml_cosem};
		auto source      = address{hdlc::lmn::address(0x3A), lmn::protocol_selector::sml_cosem};
		auto control     = control_field{frame_function::i{frame_function::sequence_number{1},
                                                       frame_function::sequence_number{0}}};
		auto i_frame     = frame{destination, source, control, payload};

		auto bytes = i_frame.serialize();

		CHECK(bytes == real_i_frame);
	}
}

TEST_CASE("hdlc broadcast payload")
{
	using namespace hdlc;

	SUBCASE("payload parsing")
	{
		vec<u8> payload1 = {0x1A, 0x02, 0x0A, 0x01, 0x4B, 0x41, 0x49, 0x00, 0x52, 0x42, 0x50,
		                    0x49, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x4B, 0x41, 0x49, 0x00,
		                    0x52, 0x42, 0x50, 0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

		auto [meter_id, info] = lmn::deserialize_broadcast_payload(payload1);
		CHECK(info.slave_address == static_cast<lmn::address>(0x1A));
		CHECK(meter_id ==
		      std::array<u8, 10>{0x0A, 0x01, 0x4B, 0x41, 0x49, 0x00, 0x52, 0x42, 0x50, 0x49});
		CHECK(info.sensor_id ==
		      std::array<u8, 10>{0x0A, 0x01, 0x4B, 0x41, 0x49, 0x00, 0x52, 0x42, 0x50, 0x49});
		CHECK(info.state == lmn::slave_info::cardinality::one_to_one_to_one);
	}

	SUBCASE("payload writing")
	{
		// create map
		lmn::slave_map slaves;
		slaves[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}] = {static_cast<lmn::address>(0x12),
		                                           {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
		                                           lmn::slave_info::cardinality::one_to_one_to_one,
		                                           {}};
		slaves[{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}] = {static_cast<lmn::address>(0x34),
		                                           {10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
		                                           lmn::slave_info::cardinality::one_to_one_to_one,
		                                           {}};
		// call function
		auto payload = lmn::serialize_broadcast_payload(slaves, false);
		// order is not relevant
		vec<u8> payload1{0x12, 0x00, 1, 2, 3, 4, 5, 6, 7, 8,  9,    10,   0x00, 0x00, 0x00, 0x00,
		                 1,    2,    3, 4, 5, 6, 7, 8, 9, 10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		vec<u8> payload2{0x34, 0x00, 10, 9, 8, 7, 6, 5, 4, 3, 2,    1,    0x00, 0x00, 0x00, 0x00,
		                 10,   9,    8,  7, 6, 5, 4, 3, 2, 1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		CHECK(std::search(payload.begin(), payload.end(), payload1.begin(), payload1.end()) !=
		      payload.end());
		CHECK(std::search(payload.begin(), payload.end(), payload2.begin(), payload2.end()) !=
		      payload.end());
	}
}

class serial_port_mock : public serial::serial_interface
{
public:
	MAKE_MOCK1(read_single_frame, vec<u8>(std::chrono::milliseconds), override);
	MAKE_MOCK1(read_for, vec<vec<u8>>(std::chrono::milliseconds), override);
	MAKE_MOCK1(write, void(vec<u8>&&), override);
};

TEST_CASE("hdlc master broadcast")
{
	using namespace hdlc::lmn;
	using std::chrono::milliseconds;
	serial_port_mock serial;
	master hdlc_master(serial);

	REQUIRE(hdlc_master.next_broadcast_state() == protocol_selector::query_new);
	REQUIRE(hdlc_master.connected_slaves().empty());

	vec<u8> broadcast_frame;
	REQUIRE_CALL(serial, write(ANY(vec<u8>)))  // write is called
	    .LR_SIDE_EFFECT(broadcast_frame = _1); // make written frame accessable

	FORBID_CALL(serial, read_single_frame(ANY(milliseconds)));

	SUBCASE("no meters are known and none are responding to the first broadcast")
	{
		// read_for is called with any argument and returns an empty vector
		REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(vec<vec<u8>>{});

		hdlc_master.do_broadcast();

		CHECK(broadcast_frame.size() == 11);
		CHECK(hdlc_master.next_broadcast_state() == protocol_selector::check_missing);
		CHECK(hdlc_master.connected_slaves().empty());

		SUBCASE("nothing happens at the second broadcast, either")
		{
			REQUIRE_CALL(serial, write(ANY(vec<u8>))).LR_SIDE_EFFECT(broadcast_frame = _1);
			REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(vec<vec<u8>>{});

			hdlc_master.do_broadcast();

			CHECK(broadcast_frame.size() == 11);
			CHECK(hdlc_master.next_broadcast_state() == protocol_selector::query_new);
			CHECK(hdlc_master.connected_slaves().empty());
		}
	}

	SUBCASE("responding meters are memorized")
	{
		vec<vec<u8>> broadcast_responses1{
		    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0x88, 0x03, 0x13, 0xB2, 0x30, //
		     0x44, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD,
		     0x21, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84,
		     0x01, 0x4B, 0xAD, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
		     0x34, 0x83, 0x7E},
		    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0xF2, 0x03, 0x13, 0x10, 0xC3, //
		     0x79, 0x00, 0x0A, 0x01, 0x45, 0x4C, 0x56, 0x79, 0x00, 0x34, 0x79,
		     0x03, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x45, 0x4C, 0x56, 0x79,
		     0x00, 0x34, 0x79, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
		     0x26, 0x0C, 0x7E},
		    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0x7C, 0x03, 0x13, 0xE7, 0xDF, //
		     0x3E, 0x00, 0x0A, 0x01, 0x4A, 0x53, 0x4E, 0x39, 0x04, 0x73, 0x19,
		     0xBB, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x4A, 0x53, 0x4E, 0x39,
		     0x04, 0x73, 0x19, 0xBB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
		     0x79, 0x03, 0x7E},
		    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0xFA, 0x03, 0x13, 0xD2, 0x05, //
		     0x7D, 0x00, 0x0A, 0x01, 0x44, 0x56, 0x44, 0x4E, 0x05, 0x47, 0xAF,
		     0xC4, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x44, 0x56, 0x44, 0x4E,
		     0x05, 0x47, 0xAF, 0xC4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
		     0x5E, 0xEF, 0x7E},
		    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0x52, 0x03, 0x13, 0xC7, 0xCC, //
		     0x29, 0x00, 0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA, 0x02, 0x2D, 0x20,
		     0x03, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA,
		     0x02, 0x2D, 0x20, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
		     0x7E, 0x1C, 0x7E},
		};

		REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(broadcast_responses1);

		hdlc_master.do_broadcast();

		CHECK(hdlc_master.next_broadcast_state() == protocol_selector::check_missing);
		CHECK(hdlc_master.connected_slaves().size() == broadcast_responses1.size());
		CHECK(contains(hdlc_master.connected_slaves(),
		               device_id{0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD, 0x21}));
		CHECK(contains(hdlc_master.connected_slaves(),
		               device_id{0x0A, 0x01, 0x45, 0x4C, 0x56, 0x79, 0x00, 0x34, 0x79, 0x03}));
		CHECK(contains(hdlc_master.connected_slaves(),
		               device_id{0x0A, 0x01, 0x4A, 0x53, 0x4E, 0x39, 0x04, 0x73, 0x19, 0xBB}));
		CHECK(contains(hdlc_master.connected_slaves(),
		               device_id{0x0A, 0x01, 0x44, 0x56, 0x44, 0x4E, 0x05, 0x47, 0xAF, 0xC4}));
		CHECK(contains(hdlc_master.connected_slaves(),
		               device_id{0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA, 0x02, 0x2D, 0x20, 0x03}));

		SUBCASE("all known meters are checked at the second broadcast")
		{
			vec<vec<u8>> broadcast_responses2{
			    {0x7E, 0xA0, 0x2B, 0x02, 0x05, 0x52, 0x05, 0x13, 0x8D, 0xD3, 0x29, 0x01,
			     0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA, 0x02, 0x2D, 0x20, 0x03, 0x00, 0x00,
			     0x00, 0x00, 0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA, 0x02, 0x2D, 0x20, 0x03,
			     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4A, 0xD9, 0x7E},
			    {0x7E, 0xA0, 0x2B, 0x02, 0x05, 0x7C, 0x05, 0x13, 0xAD, 0xC0, 0x3E, 0x02,
			     0x0A, 0x01, 0x4A, 0x53, 0x4E, 0x39, 0x04, 0x73, 0x19, 0xBB, 0x00, 0x00,
			     0x00, 0x00, 0x0A, 0x01, 0x4A, 0x53, 0x4E, 0x39, 0x04, 0x73, 0x19, 0xBB,
			     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x81, 0x7E},
			    {0x7E, 0xA0, 0x2B, 0x02, 0x05, 0x88, 0x05, 0x13, 0xF8, 0x2F, 0x44, 0x03,
			     0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD, 0x21, 0x00, 0x00,
			     0x00, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD, 0x21,
			     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79, 0xC4, 0x7E},
			    {0x7E, 0xA0, 0x2B, 0x02, 0x05, 0xF2, 0x05, 0x13, 0x5A, 0xDC, 0x79, 0x04,
			     0x0A, 0x01, 0x45, 0x4C, 0x56, 0x79, 0x00, 0x34, 0x79, 0x03, 0x00, 0x00,
			     0x00, 0x00, 0x0A, 0x01, 0x45, 0x4C, 0x56, 0x79, 0x00, 0x34, 0x79, 0x03,
			     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC5, 0x00, 0x7E},
			    {0x7E, 0xA0, 0x2B, 0x02, 0x05, 0xFA, 0x05, 0x13, 0x98, 0x1A, 0x7D, 0x05,
			     0x0A, 0x01, 0x44, 0x56, 0x44, 0x4E, 0x05, 0x47, 0xAF, 0xC4, 0x00, 0x00,
			     0x00, 0x00, 0x0A, 0x01, 0x44, 0x56, 0x44, 0x4E, 0x05, 0x47, 0xAF, 0xC4,
			     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x89, 0x26, 0x7E}};

			REQUIRE_CALL(serial, write(ANY(vec<u8>))).LR_SIDE_EFFECT(broadcast_frame = _1);
			REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(broadcast_responses2);

			hdlc_master.do_broadcast();

			CHECK(hdlc_master.connected_slaves().size() == 5);

			// [LMN_0063]: given timeslots are ascending, starting from 1
			CHECK(broadcast_frame.size() == (13 + 5 * 32));
			CHECK(broadcast_frame[10 + 1 + 0 * 32] == 0x01);
			CHECK(broadcast_frame[10 + 1 + 1 * 32] == 0x02);
			CHECK(broadcast_frame[10 + 1 + 2 * 32] == 0x03);
			CHECK(broadcast_frame[10 + 1 + 3 * 32] == 0x04);
			CHECK(broadcast_frame[10 + 1 + 4 * 32] == 0x05);

			SUBCASE("timeslots are zero at next query_new broadcast")
			{
				REQUIRE_CALL(serial, write(ANY(vec<u8>))).LR_SIDE_EFFECT(broadcast_frame = _1);
				REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(vec<vec<u8>>{});

				hdlc_master.do_broadcast();

				CHECK(hdlc_master.connected_slaves().size() == 5);

				// [LMN_0060]
				CHECK(broadcast_frame.size() == (13 + 5 * 32));
				CHECK(broadcast_frame[10 + 1 + 0 * 32] == 0x00);
				CHECK(broadcast_frame[10 + 1 + 1 * 32] == 0x00);
				CHECK(broadcast_frame[10 + 1 + 2 * 32] == 0x00);
				CHECK(broadcast_frame[10 + 1 + 3 * 32] == 0x00);
				CHECK(broadcast_frame[10 + 1 + 4 * 32] == 0x00);
			}

			SUBCASE("meters choosing an address that is already in use are ignored")
			{
				vec<vec<u8>> broadcast_response2{
				    {0x7E, 0xA0, 0x2B, 0x02, 0x03, 0x88, 0x03, 0x13, 0xB2, 0x30, //
				     0x44, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD,
				     0x21, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84,
				     0x01, 0x4B, 0xAD, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //
				     0x34, 0x83, 0x7E}};

				REQUIRE_CALL(serial, write(ANY(vec<u8>)));
				REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(broadcast_response2);

				hdlc_master.do_broadcast();

				CHECK(hdlc_master.connected_slaves().size() == 5);
			}
		}
	}

	SUBCASE("slaves responding with the same address are ignored/not enlisted")
	{
		hdlc::frame slave1 = {hdlc::address{address::smgw, protocol_selector::query_new},
		                      hdlc::address{address{0x33}, protocol_selector::query_new},
		                      hdlc::frame_function::ui{},
		                      {0x33, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84, 0x01, 0x4B, 0xAD,
		                       0x21, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x4E, 0x56, 0x53, 0x84,
		                       0x01, 0x4B, 0xAD, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
		hdlc::frame slave2 = {hdlc::address{address::smgw, protocol_selector::query_new},
		                      hdlc::address{address{0x33}, protocol_selector::query_new},
		                      hdlc::frame_function::ui{},
		                      {0x33, 0x00, 0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA, 0x02, 0x2D, 0x20,
		                       0x03, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x01, 0x56, 0x50, 0x41, 0xBA,
		                       0x02, 0x2D, 0x20, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

		vec<vec<u8>> collision_response;
		collision_response.push_back(slave1.serialize());
		collision_response.push_back(slave2.serialize());

		REQUIRE_CALL(serial, read_for(ANY(milliseconds))).RETURN(collision_response);
		hdlc_master.do_broadcast();

		CHECK(hdlc_master.connected_slaves().size() == 0);
	}
}

class transceive_mock
{
public:
	MAKE_MOCK1(transceive, vec<u8>(vec<u8>&&));
};

TEST_CASE("hdlc connection establishing")
{
	using namespace hdlc::lmn;
	transceive_mock m;
	auto transceive    = [&m](vec<u8>&& v) { return m.transceive(std::forward<decltype(v)>(v)); };
	auto slave_address = address{0x55};
	auto session_protocol = protocol_selector::tls_sml_cosem;

	// TODO: test wrong slave address/protocol

	GIVEN("no active session")
	{
		std::optional<session> s;

		WHEN("establish_connection is called and the response is correct")
		{
			hdlc::frame snrm_response{hdlc::address{address::smgw, session_protocol},
			                          hdlc::address{slave_address, session_protocol},
			                          hdlc::frame_function::ua{},
			                          {}};

			vec<u8> snrm_request;
			REQUIRE_CALL(m, transceive(ANY(vec<u8>)))
			    .LR_SIDE_EFFECT(snrm_request = _1)
			    .RETURN(snrm_response.serialize());

			auto new_session = establish_connection(slave_address, session_protocol, s, transceive);

			THEN("only a correct snrm frame is transmitted")
			{
				auto frame = hdlc::deserialize_frame(snrm_request);
				CHECK(std::holds_alternative<hdlc::frame_function::snrm>(frame.control));
				CHECK(frame.destination.dynamic_address == slave_address);
				CHECK(frame.destination.session_protocol == session_protocol);
				CHECK(frame.payload.empty());
				CHECK(frame.source.dynamic_address == address::smgw);
			}

			THEN("the active session is set") { CHECK(new_session.protocol == session_protocol); }
		}

		WHEN("establish_connection is called and the response is no ua frame")
		{
			hdlc::frame wrong_response{hdlc::address{address::smgw, session_protocol},
			                           hdlc::address{slave_address, session_protocol},
			                           hdlc::frame_function::dm{},
			                           {}};

			vec<u8> snrm_request;
			REQUIRE_CALL(m, transceive(ANY(vec<u8>)))
			    .LR_SIDE_EFFECT(snrm_request = _1)
			    .RETURN(wrong_response.serialize());

			THEN("an exception is thrown")
			{
				CHECK_THROWS(establish_connection(slave_address, session_protocol, s, transceive));
			}
		}
	}

	GIVEN("an active session")
	{
		auto old_protocol        = protocol_selector::sml_cosem;
		std::optional<session> s = session{old_protocol, 3, 2};

		WHEN("establish_connection is called")
		{
			trompeloeil::sequence seq;
			hdlc::frame disc_response{hdlc::address{address::smgw, old_protocol},
			                          hdlc::address{slave_address, old_protocol},
			                          hdlc::frame_function::ua{},
			                          {}};
			hdlc::frame snrm_response{hdlc::address{address::smgw, session_protocol},
			                          hdlc::address{slave_address, session_protocol},
			                          hdlc::frame_function::ua{},
			                          {}};
			vec<u8> disc_request;
			vec<u8> snrm_request;

			REQUIRE_CALL(m, transceive(ANY(vec<u8>)))
			    .LR_SIDE_EFFECT(disc_request = _1)
			    .IN_SEQUENCE(seq)
			    .RETURN(disc_response.serialize());

			REQUIRE_CALL(m, transceive(ANY(vec<u8>)))
			    .LR_SIDE_EFFECT(snrm_request = _1)
			    .IN_SEQUENCE(seq)
			    .RETURN(snrm_response.serialize());

			auto new_session = establish_connection(slave_address, session_protocol, s, transceive);

			THEN("the old session is closed and the new session is established")
			{
				auto disc_frame = hdlc::deserialize_frame(disc_request);
				CHECK(std::holds_alternative<hdlc::frame_function::disc>(disc_frame.control));
				CHECK(disc_frame.destination.dynamic_address == slave_address);
				CHECK(disc_frame.destination.session_protocol == old_protocol);
				CHECK(disc_frame.payload.empty());
				CHECK(disc_frame.source.dynamic_address == address::smgw);

				auto snrm_frame = hdlc::deserialize_frame(snrm_request);
				CHECK(std::holds_alternative<hdlc::frame_function::snrm>(snrm_frame.control));
				CHECK(snrm_frame.destination.dynamic_address == slave_address);
				CHECK(snrm_frame.destination.session_protocol == session_protocol);
				CHECK(snrm_frame.payload.empty());
				CHECK(snrm_frame.source.dynamic_address == address::smgw);

				CHECK(new_session.protocol == session_protocol);
				CHECK(new_session.receive_sequence_variable == 0);
				CHECK(new_session.send_sequence_variable == 0);
			}
		}
		WHEN("the response to the disc frame is neither ua nor dm")
		{
			hdlc::frame disc_response{hdlc::address{address::smgw, old_protocol},
			                          hdlc::address{slave_address, old_protocol},
			                          hdlc::frame_function::frmr{},
			                          {}};

			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(disc_response.serialize());

			THEN("an exception is thrown")
			{
				CHECK_THROWS(establish_connection(slave_address, session_protocol, s, transceive));
			}
		}
	}
}

TEST_CASE("hdlc unicast")
{
	using namespace hdlc::lmn;
	transceive_mock m;
	auto transceive    = [&m](vec<u8>&& v) { return m.transceive(std::forward<decltype(v)>(v)); };
	auto slave_address = address{0x55};

	// I -> I (data available) or RR (need more data) or RNR (busy)

	GIVEN("an active session")
	{
		auto active_session = session{protocol_selector::sml_cosem, {}, {}};
		CHECK(active_session.receive_sequence_variable == 0);
		CHECK(active_session.send_sequence_variable == 0);

		WHEN("the slave responds directly with an i frame")
		{
			auto i_response = hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                              hdlc::address{slave_address, active_session.protocol},
			                              hdlc::frame_function::i{1, 0},
			                              {1, 2, 3, 4, 5}}
			                      .serialize();

			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(i_response);
			auto response_payload =
			    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive);

			THEN("the payload is returned")
			{
				CHECK(response_payload.has_value());
				CHECK(*response_payload == vec<u8>{1, 2, 3, 4, 5});
				CHECK(active_session.send_sequence_variable == 1);
				CHECK(active_session.receive_sequence_variable == 1);
			}
		}
		WHEN("the slave responds with an RR frame") // TODO: test optional return
		{
			auto rr_response =
			    hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                hdlc::address{slave_address, active_session.protocol},
			                hdlc::frame_function::rr{hdlc::frame_function::sequence_number{1}},
			                {1, 2, 3, 4, 5}}
			        .serialize();
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(rr_response);
			auto response_payload =
			    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive);

			THEN("nothing is returned (and no exception is thrown)")
			{
				CHECK(!response_payload.has_value());
				CHECK(active_session.send_sequence_variable == 1);
				CHECK(active_session.receive_sequence_variable == 0);
			}
		}
		WHEN("the slave responds with an RNR frame")
		{
			auto rnr_response =
			    hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                hdlc::address{slave_address, active_session.protocol},
			                hdlc::frame_function::rnr{hdlc::frame_function::sequence_number{1}},
			                {1, 2, 3, 4, 5}}
			        .serialize();
			auto i_response = hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                              hdlc::address{slave_address, active_session.protocol},
			                              hdlc::frame_function::i{1, 0},
			                              {1, 2, 3, 4, 5}}
			                      .serialize();
			trompeloeil::sequence seq;
			vec<u8> rr_request;
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(rnr_response).IN_SEQUENCE(seq);
			REQUIRE_CALL(m, transceive(ANY(vec<u8>)))
			    .RETURN(i_response)
			    .IN_SEQUENCE(seq)
			    .LR_SIDE_EFFECT(rr_request = _1);
			auto response_payload =
			    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive);

			THEN("an RR frame is sent")
			{
				auto rr_frame = hdlc::deserialize_frame(rr_request);
				CHECK(std::holds_alternative<hdlc::frame_function::rr>(rr_frame.control));
			}
		}
		WHEN("the slave responds with something else")
		{
			auto wrong_frame = hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                               hdlc::address{slave_address, active_session.protocol},
			                               hdlc::frame_function::frmr{},
			                               {1, 2, 3, 4, 5}}
			                       .serialize();
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(wrong_frame);

			THEN("an exception is thrown")
			{
				CHECK_THROWS(
				    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive));
			}
		}
		WHEN("the wrong slave responds")
		{
			auto wrong_frame = hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                               hdlc::address{address{8}, active_session.protocol},
			                               hdlc::frame_function::i{1, 0},
			                               {1, 2, 3, 4, 5}}
			                       .serialize();
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(wrong_frame);

			THEN("an exception is thrown")
			{
				CHECK_THROWS(
				    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive));
			}
		}
		WHEN("the slave responds with a wrong protocol selector")
		{
			auto wrong_frame =
			    hdlc::frame{hdlc::address{address::smgw, protocol_selector::tls_sml_cosem},
			                hdlc::address{slave_address, protocol_selector::tls_sml_cosem},
			                hdlc::frame_function::i{1, 0},
			                {1, 2, 3, 4, 5}}
			        .serialize();
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(wrong_frame);

			THEN("an exception is thrown")
			{
				CHECK_THROWS(
				    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive));
			}
		}
		WHEN("the slave responds with wrong sequence numbers")
		{
			auto wrong_frame = hdlc::frame{hdlc::address{address::smgw, active_session.protocol},
			                               hdlc::address{slave_address, active_session.protocol},
			                               hdlc::frame_function::i{4, 7},
			                               {1, 2, 3, 4, 5}}
			                       .serialize();
			REQUIRE_CALL(m, transceive(ANY(vec<u8>))).RETURN(wrong_frame);

			THEN("an exception is thrown")
			{
				CHECK_THROWS(
				    unicast(slave_address, active_session, vec<u8>{1, 2, 3, 4, 5}, transceive));
			}
		}
	}
}
