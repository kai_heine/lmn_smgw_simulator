#define DOCTEST_CONFIG_IMPLEMENT

#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#include <smgw/log.h>

int main(int argc, char** argv)
{
	doctest::Context context;
	context.applyCommandLine(argc, argv);

	smgw::log::init();
	spdlog::set_level(spdlog::level::off);

	int result = context.run();

	spdlog::drop_all();
	return result;
}
