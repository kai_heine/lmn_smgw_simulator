add_executable(test_executable
	test_main.cpp
	hdlc_test.cpp
	sml_test.cpp
    sym_test.cpp

	test_config.h
)
target_compile_features(test_executable PUBLIC cxx_std_17)
target_link_libraries(test_executable PUBLIC lmn_wired doctest trompeloeil)

add_test(${PROJECT_NAME}_test test_executable)
