#include "test_config.h"
#include <smgw/sym.h>
using namespace smgw;
using namespace smgw::sym;

TEST_CASE("key derivation")
{
	key master_key{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	smgw::device_id meter_id{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	smgw::u32 transmission_counter = 0x12345678;

	auto keypair1 = derive_keys(master_key, message_direction::gateway_to_meter,
	                            transmission_counter, meter_id);
	REQUIRE(keypair1.enc != keypair1.mac);
	auto keypair2 = derive_keys(master_key, message_direction::gateway_to_meter,
	                            transmission_counter, meter_id);
	REQUIRE(keypair1.enc == keypair2.enc);
	REQUIRE(keypair1.mac == keypair2.mac);
	auto keypair3 = derive_keys(master_key, message_direction::gateway_to_meter,
	                            transmission_counter - 1, meter_id);
	REQUIRE(keypair3.enc != keypair1.enc);
}

TEST_CASE("symmetric encryption and decryption")
{
	key test_key{};
	vec<u8> plaintext{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	auto ciphertext = encrypt_data(test_key, plaintext);
	REQUIRE(ciphertext.size() == 16);
	REQUIRE(ciphertext != plaintext);
	auto ciphertext2 = encrypt_data(test_key, plaintext);
	REQUIRE(ciphertext == ciphertext2);
	key other_key{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	auto ciphertext3 = encrypt_data(other_key, plaintext);
	REQUIRE(ciphertext3 != ciphertext);

	auto decrypted = decrypt_data(test_key, ciphertext);
	REQUIRE(decrypted == plaintext);
}

TEST_CASE("mac calculation")
{
	key test_key{};
	vec<u8> data{1, 2, 3, 4, 5, 6, 7, 8};
	auto mac = calculate_mac(test_key, data);
	REQUIRE(mac.size() == 16);
	auto mac2 = calculate_mac(test_key, data);
	REQUIRE(mac == mac2);
	data[0]   = 0xff;
	auto mac3 = calculate_mac(test_key, data);
	REQUIRE(mac3 != mac);
}

TEST_CASE("message serialization")
{
	key mk{};
	device_id meter_id{};
	message msg{0x01, 0xffffffff, vec<u8>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};
	auto buf = serialize_message(msg, message_direction::meter_to_gateway, mk, meter_id);
	REQUIRE(buf.size() == 1 + 4 + 4 + 16 + 16);
	REQUIRE(buf[0] == 0x01);
	REQUIRE(buf[4] == buf.size());
	REQUIRE(buf[8] == 0xff);

	auto parsed_msg = deserialize_message(buf, message_direction::meter_to_gateway, mk, meter_id);
	REQUIRE(parsed_msg.command == msg.command);
	REQUIRE(parsed_msg.transmission_counter == msg.transmission_counter);
	REQUIRE(*parsed_msg.plaintext_data == *msg.plaintext_data);
}

TEST_CASE("keygen")
{
	generate_cert_and_key("testname", iana_named_curve::secp256r1);
	get_gateway_cert_key();
}

TEST_CASE("list of algorithms")
{
	// ff ff 00 01 01 c0 23 02 00 17 00 1a
	vec<u8> test_data{0xff, 0xff, 0x00, 0x01, 0x01, 0xc0, 0x23, 0x02, 0x00, 0x17, 0x00, 0x1a};
	auto algorithms = parse_supported_algorithms(test_data);
}
