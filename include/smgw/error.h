#ifndef SMGW_ERROR_H
#define SMGW_ERROR_H

#include <system_error>

// TODO: generic error_conditions for high-level error lookup

namespace smgw
{

enum class general_errc
{
	openssl_error = 1
};

std::error_code make_error_code(general_errc);

enum class serial_errc
{
	timeout = 1,
	invalid_frame
};

std::error_code make_error_code(serial_errc);

enum class hdlc_errc
{
	invalid_control_field_format = 1,
	invalid_checksum,
	invalid_address,
	slave_not_connected,
	unexpected_response_frame
};

std::error_code make_error_code(hdlc_errc);

enum class tls_errc
{
	missing_certs_and_keys = 1
};

std::error_code make_error_code(tls_errc);

enum class sml_errc
{
	differing_object_id = 1,
	object_attributes_missing,
	unsupported_obis_number,
	unsupported_cosem_class,
	invalid_response,

	// attention codes
	unspecified_failure                      = 0xFE00,
	unknown_sml_identifier                   = 0xFE01,
	attributes_not_writable                  = 0xFE05,
	attributes_not_readable                  = 0xFE06,
	value_supplied_outside_permissable_range = 0xFE09,
	request_not_executed                     = 0xFE0A,
	temporarily_unavailable                  = 0xFE15
};

std::error_code make_error_code(sml_errc);
const std::error_category& get_sml_category() noexcept;

enum class sym_errc
{
	invalid_mac = 1,
	unexpected_frame
};

std::error_code make_error_code(sym_errc);

} // namespace smgw

// type traits
namespace std
{
template <>
struct is_error_code_enum<smgw::general_errc> : true_type
{};
template <>
struct is_error_code_enum<smgw::serial_errc> : true_type
{};
template <>
struct is_error_code_enum<smgw::hdlc_errc> : true_type
{};
template <>
struct is_error_code_enum<smgw::tls_errc> : true_type
{};
template <>
struct is_error_code_enum<smgw::sml_errc> : true_type
{};
template <>
struct is_error_code_enum<smgw::sym_errc> : true_type
{};
} // namespace std

#endif // SMGW_ERROR_H
