#ifndef SMGW_OBIS_H
#define SMGW_OBIS_H

#include <array>
#include <cstdint>
#include <smgw/utility/utility.h>

namespace smgw::obis
{
enum class code : std::uint64_t
{
	// class data with unsigned32 value
	time   = 0x01005E310001,
	status = 0x0100600500FF,

	// class data with unsigned16 value
	maximum_fragment_size = 0x01005E310009,

	// class data with octet-string value
	device_class      = 0x01005E310105,
	manufacturer_id   = 0x010060320101,
	device_id         = 0x0100600100FF,
	public_key        = 0x01005E310002,
	device_fw_version = 0x010000020000,
	fw_checksum       = 0x0100605A0201,
	meter_cert        = 0x01005E310003,
	smgw_cert         = 0x01005E310008,

	// class data with boolean value
	crypto_reset = 0x01005E310007,

	// class advanced_extended_register
	active_energy_total   = 0x0100010800FF,
	reactive_energy_total = 0x0100020800FF,

	// class register
	voltage_l1_instantaneous   = 0x0100200700FF,
	voltage_l2_instantaneous   = 0x0100340700FF,
	voltage_l3_instantaneous   = 0x0100480700FF,
	active_power_instantaneous = 0x0100100700FF,
};

} // namespace smgw::obis

#endif // SMGW_OBIS_H
