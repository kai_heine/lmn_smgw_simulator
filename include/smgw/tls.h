#ifndef SMGW_TLS_H
#define SMGW_TLS_H

#include "hdlc/lmn.h"
#include "meter_profile.h"
#include "utility/transceive.h"
#include "utility/utility.h"
#include <map>
#include <memory>
#include <openssl/ssl.h>
#include <optional>

namespace smgw::tls
{

struct ssl_deleter
{
	void operator()(SSL* ssl) { SSL_free(ssl); }

	void operator()(SSL_CTX* ctx) { SSL_CTX_free(ctx); }
};

using ssl_ptr     = std::unique_ptr<SSL, ssl_deleter>;
using ssl_ctx_ptr = std::unique_ptr<SSL_CTX, ssl_deleter>;

struct session
{
	ssl_ptr tls_session;
};

// manages all sessions
// creates and destroys ssl objects
class manager
{
public:
	manager(const meter_profile_store& meter_profiles);

	session& get_session(const device_id& meter_id);
	void delete_session(const device_id& meter_id);

private:
	ssl_ctx_ptr tls_context;
	std::map<device_id, session> active_sessions;
	const meter_profile_store& meter_profiles;
};

class transceiver
{
public:
	transceiver(session& active_session_, transceive_func transceive_)
	    : active_session{active_session_}, transceive{std::move(transceive_)}
	{}
	std::optional<vec<u8>> operator()(const device_id& meter_id, std::optional<vec<u8>>&& payload);

private:
	session& active_session;
	transceive_func transceive;
};

} // namespace smgw::tls

#endif // SMGW_TLS_H
