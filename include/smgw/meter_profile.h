#ifndef SMGW_METER_PROFILE_H
#define SMGW_METER_PROFILE_H

#include "utility/utility.h"
#include <functional>
#include <map>
#include <optional>
#include <string>

namespace smgw
{

struct meter_profile
{
	vec<u8> sensor_key;
	std::optional<u16> max_fragment_length;
};

using meter_profile_map        = std::map<device_id, meter_profile>;
using meter_profile_write_func = std::function<void(meter_profile_map const&)>;

class meter_profile_store
{
public:
	meter_profile_store(meter_profile_map&& meter_profiles, meter_profile_write_func f = nullptr);
	[[nodiscard]] std::optional<meter_profile> get(device_id const& meter_id) const;
	void update(device_id const& meter_id, meter_profile&& profile);

private:
	meter_profile_map meter_profiles_;
	meter_profile_write_func write_meter_profiles_;
};

device_id device_id_str_to_hex(std::string device_id_str);
std::string device_id_hex_to_str(device_id const& id);

} // namespace smgw

#endif // SMGW_METER_PROFILE_H
