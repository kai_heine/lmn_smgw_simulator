#ifndef SMGW_HDLC_H
#define SMGW_HDLC_H

#include "hdlc/address.h"
#include "hdlc/control_field.h"
#include "hdlc/frame.h"
#include "hdlc/lmn/master.h"

// Specialized implementation of HDLC for the LMN
// All LMN-specific extensions and mechanisms are grouped in namespace lmn.

#endif // SMGW_HDLC_H
