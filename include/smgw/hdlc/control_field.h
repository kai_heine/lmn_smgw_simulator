#ifndef SMGW_HDLC_CONTROL_FIELD_H
#define SMGW_HDLC_CONTROL_FIELD_H

#include "../utility/utility.h"
#include <string_view>
#include <variant>

namespace smgw::hdlc
{

namespace frame_function
{

using sequence_number = bounded<u8, 0, 7>;

// Information
struct i
{
	constexpr i() noexcept = default;

	constexpr i(sequence_number rsn, sequence_number ssn) noexcept
	    : send_sequence_number{ssn}, receive_sequence_number{rsn}
	{}

	constexpr i(u8 control_field) noexcept
	    : send_sequence_number((control_field & 0b1110) >> 1),
	      receive_sequence_number(control_field >> 5)
	{
		assert((control_field & 0b1) == mask);
	}

	sequence_number send_sequence_number;
	sequence_number receive_sequence_number;
	constexpr static u8 mask               = 0b00000000;
	constexpr static std::string_view name = "I";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Receive ready
struct rr
{
	constexpr rr() noexcept = default;

	constexpr rr(sequence_number rsn) noexcept : receive_sequence_number{rsn} {}

	constexpr rr(u8 control_field) noexcept : receive_sequence_number(control_field >> 5)
	{
		assert((control_field & 0b1111) == mask);
	}

	sequence_number receive_sequence_number;
	constexpr static u8 mask               = 0b00000001;
	constexpr static std::string_view name = "RR";

	[[nodiscard]] std::uint8_t serialize() const noexcept;
};

// Receive not ready
struct rnr
{
	constexpr rnr() noexcept = default;

	constexpr rnr(sequence_number rsn) noexcept : receive_sequence_number{rsn} {}

	constexpr rnr(u8 control_field) noexcept : receive_sequence_number(control_field >> 5)
	{
		assert((control_field & 0b1111) == 0b00000101);
	}

	sequence_number receive_sequence_number;
	constexpr static u8 mask               = 0b00000101;
	constexpr static std::string_view name = "RNR";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Set normal response mode
struct snrm
{
	constexpr static u8 mask               = 0b10000011;
	constexpr static std::string_view name = "SNRM";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Disconnect
struct disc
{
	constexpr static u8 mask               = 0b01000011;
	constexpr static std::string_view name = "DISC";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Unnumbered acknowledgement
struct ua
{
	constexpr static u8 mask               = 0b01100011;
	constexpr static std::string_view name = "UA";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Disconnected mode
struct dm
{
	constexpr static u8 mask               = 0b00001111;
	constexpr static std::string_view name = "DM";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Frame reject
struct frmr
{
	constexpr static u8 mask               = 0b10000111;
	constexpr static std::string_view name = "FRMR";

	[[nodiscard]] u8 serialize() const noexcept;
};

// Unnumbered information
struct ui
{
	constexpr static u8 mask               = 0b00000011;
	constexpr static std::string_view name = "UI";

	[[nodiscard]] u8 serialize() const noexcept;
};

} // namespace frame_function

using control_field = std::variant<frame_function::i, frame_function::rr, frame_function::rnr,
                                   frame_function::snrm, frame_function::disc, frame_function::ua,
                                   frame_function::dm, frame_function::frmr, frame_function::ui>;

control_field deserialize_control_field(u8 ctrl);

} // namespace smgw::hdlc

#endif // SMGW_HDLC_CONTROL_FIELD_H
