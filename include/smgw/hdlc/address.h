#ifndef SMGW_HDLC_ADDRESS_H
#define SMGW_HDLC_ADDRESS_H

#include "../utility/utility.h"
#include "lmn/address.h"
#include <array>

namespace smgw::hdlc
{

struct address
{
	lmn::address dynamic_address;
	lmn::protocol_selector session_protocol;

	[[nodiscard]] std::array<u8, 2> serialize() const noexcept;
};

address deserialize_address(u8 msb, u8 lsb);

} // namespace smgw::hdlc

#endif // SMGW_HDLC_ADDRESS_H
