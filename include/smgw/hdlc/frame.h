#ifndef SMGW_HDLC_FRAME_H
#define SMGW_HDLC_FRAME_H

#include "../utility/utility.h"
#include "address.h"
#include "control_field.h"

namespace smgw::hdlc
{

namespace constants
{
constexpr u8 flag                = 0x7e;
constexpr u8 frame_format_type_3 = 0xA0;
} // namespace constants

namespace frame_index
{
constexpr std::size_t start_flag       = 0x00;
constexpr std::size_t frame_format_msb = 0x01;
constexpr std::size_t frame_format_lsb = 0x02;
constexpr std::size_t dst_adr_msb      = 0x03;
constexpr std::size_t dst_adr_lsb      = 0x04;
constexpr std::size_t src_adr_msb      = 0x05;
constexpr std::size_t src_adr_lsb      = 0x06;
constexpr std::size_t ctrl_field       = 0x07;
constexpr std::size_t hdr_crc_msb      = 0x08;
constexpr std::size_t hdr_crc_lsb      = 0x09;
constexpr std::size_t payload          = 0x0A;
} // namespace frame_index

// precondition: the range [first..last] is at least 3 bytes long and begins with a flag sequence
// postcondition: frame length
template <typename Iterator>
std::size_t frame_length(Iterator first, [[maybe_unused]] Iterator last)
{
	assert(std::distance(first, last) >= 3);
	assert(*first == constants::flag);

	return *(first + 2) + ((*(first + 1) & 0xf) << 8) + 2;
}

template <typename Container>
std::size_t frame_length(const Container& frame)
{
	return frame_length(frame.begin(), frame.end());
}

struct frame
{
	address destination;
	address source;
	control_field control;
	vec<u8> payload;

	[[nodiscard]] vec<u8> serialize() const;
};

frame deserialize_frame(const vec<u8>& frame_bytes);

vec<vec<u8>> split_frames(const vec<u8>& buffer);

} // namespace smgw::hdlc

#endif // SMGW_HDLC_FRAME_H
