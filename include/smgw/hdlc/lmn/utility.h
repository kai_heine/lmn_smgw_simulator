#ifndef SMGW_HDLC_LMN_UTILITY_H
#define SMGW_HDLC_LMN_UTILITY_H

#include "../../utility/utility.h"
#include "../frame.h"

namespace smgw::hdlc::lmn
{

frame make_snrm_frame(address destination, protocol_selector protocol);

frame make_i_frame(address destination, protocol_selector protocol,
                   frame_function::sequence_number ssn, frame_function::sequence_number rsn,
                   vec<u8>&& payload);

frame make_ui_frame(protocol_selector protocol, vec<u8>&& payload);

frame make_disc_frame(address destination, protocol_selector protocol);

frame make_rr_frame(address destination, protocol_selector protocol,
                    frame_function::sequence_number rsn);
frame make_rnr_frame(address destination, protocol_selector protocol,
                     frame_function::sequence_number rsn);

} // namespace smgw::hdlc::lmn

#endif // SMGW_HDLC_LMN_UTILITY_H
