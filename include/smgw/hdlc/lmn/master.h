#ifndef SMGW_HDLC_LMN_MASTER_H
#define SMGW_HDLC_LMN_MASTER_H

#include "../../serial/serial_interface.h"
#include "../../utility/utility.h"
#include "../control_field.h"
#include "../lmn.h"
#include "address.h"
#include <functional>
#include <map>
#include <optional>
#include <shared_mutex>

namespace smgw::hdlc::lmn
{

struct session
{
	protocol_selector protocol;

	// sequence number of the next in-sequence I frame to be transmitted
	frame_function::sequence_number send_sequence_variable;

	// sequence number of the next in-sequence I frame expected to be received
	frame_function::sequence_number receive_sequence_variable;
};

struct slave_info
{
	address slave_address;
	device_id sensor_id;
	enum class cardinality : u16
	{
		one_to_one_to_one = 0x0000,
		one_to_one_to_n   = 0x0001
	} state;

	std::optional<session> active_session;
};

// map is actually faster than unordered_map for less than 10 keys (most cases)
using slave_map = std::map<device_id, slave_info>;

class master
{
public:
	master(serial::serial_interface& port) : serial(port) {}

	void do_broadcast();
	std::optional<vec<u8>> do_unicast(const device_id& meter_id, std::optional<vec<u8>>&& payload,
	                                  protocol_selector protocol);

	slave_map connected_slaves() const noexcept;
	void delete_session(const device_id& meter_id);

	protocol_selector next_broadcast_state() const noexcept;

private:
	protocol_selector next_broadcast_state_{protocol_selector::query_new};
	serial::serial_interface& serial;
	slave_map connected_slaves_;
	mutable std::shared_mutex broadcast_mutex;
};

template <protocol_selector protocol>
class unicast_transceiver
{
public:
	unicast_transceiver(master& m) : master_{m} {}

	std::optional<vec<u8>> operator()(const device_id& meter_id, std::optional<vec<u8>>&& payload)
	{
		return master_.do_unicast(meter_id, std::forward<decltype(payload)>(payload), protocol);
	}

private:
	master& master_;
};

vec<u8> serialize_broadcast_payload(const slave_map& connected_slaves, bool assign_time_slots);

slave_map::value_type deserialize_broadcast_payload(const vec<u8>& broadcast_payload);

slave_map update_slave_map(slave_map&& connected_slaves, protocol_selector broadcast_state,
                           const vec<vec<u8>>& broadcast_responses);

using transceive_func = std::function<vec<u8>(vec<u8>&&)>;

session establish_connection(address destination, protocol_selector protocol,
                             std::optional<session> active_session, transceive_func transceive);

std::optional<vec<u8>> unicast(address destination, session& active_session,
                               std::optional<vec<u8>>&& payload, transceive_func transceive);

} // namespace smgw::hdlc::lmn

#endif // SMGW_HDLC_LMN_MASTER_H
