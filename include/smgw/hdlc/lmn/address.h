#ifndef SMGW_HDLC_LMN_ADDRESS_H
#define SMGW_HDLC_LMN_ADDRESS_H

#include "../../utility/utility.h"

namespace smgw::hdlc::lmn
{
enum class address : u8
{
	forbidden     = 0x00,
	smgw          = 0x01,
	slave_default = 0x02,
	broadcast     = 0x7f
	// 0x80..0xff: impossible due to 7-byte encoding
};

// TODO: rename to service access point?
enum class protocol_selector : u8
{
	// 0x00: reserved

	// I frames:
	tls_sml_cosem = 0x01,
	tls           = 0x02, // not implemented
	sml_cosem     = 0x03,
	sml_edl       = 0x04, // not implemented
	sml_sym2      = 0x05, // not implemented
	sym           = 0x06,
	tls_sml_edl   = 0x07, // not implemented
	tls_sml_sym2  = 0x08, // not implemented

	// 0x09..0x6f: reserved
	// 0x70..0x7f: application-specific payloads
	// 0x80..0xff: impossible due to 7-byte encoding

	// UI frames (broadcast):
	query_new     = 0x01,
	check_missing = 0x02
};

} // namespace smgw::hdlc::lmn

#endif // SMGW_HDLC_LMN_ADDRESS_H
