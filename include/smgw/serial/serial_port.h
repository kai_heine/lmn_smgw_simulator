#ifndef SMGW_SERIAL_PORT_H
#define SMGW_SERIAL_PORT_H

#include "serial_interface.h"
#ifdef _WIN32
#include <sdkddkver.h>
#endif
#include <boost/asio/serial_port.hpp>
#include <chrono>
#include <mutex>
#include <string>

namespace smgw::serial
{

class serial_port : public serial_interface
{
public:
	serial_port(const std::string& device);

	vec<u8>
	read_single_frame(std::chrono::milliseconds timeout = std::chrono::milliseconds(1)) override;

	vec<vec<u8>> read_for(std::chrono::milliseconds duration) override;

	void write(vec<u8>&& frame) override;

private:
	boost::asio::io_context io_ctx;
	boost::asio::serial_port port{io_ctx};
	std::mutex serial_mutex;
};

} // namespace smgw

#endif // SMGW_SERIAL_PORT_H
