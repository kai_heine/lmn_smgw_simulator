#ifndef SMGW_SERIAL_INTERFACE_H
#define SMGW_SERIAL_INTERFACE_H

#include "../utility/utility.h"
#include <chrono>

namespace smgw::serial
{

class serial_interface
{
public:
	virtual ~serial_interface()                                          = default;
	virtual vec<u8> read_single_frame(std::chrono::milliseconds timeout) = 0;
	virtual vec<vec<u8>> read_for(std::chrono::milliseconds duration)    = 0;
	virtual void write(vec<u8>&& frame)                                  = 0;
	// TODO: transceive wrapper functions? (here or part of hdlc?)
};

} // namespace smgw::serial

#endif // SMGW_SERIAL_INTERFACE_H
