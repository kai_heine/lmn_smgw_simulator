#ifndef SMGW_SERIAL_CONFIG_H
#define SMGW_SERIAL_CONFIG_H

#include <string>

namespace smgw::serial
{

struct configuration
{
	std::string port_name{"/dev/ttyUSB0"};
};

} // namespace smgw::serial

#endif // SMGW_SERIAL_CONFIG_H
