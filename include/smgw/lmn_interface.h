#ifndef SMGW_LMN_INTERFACE_H
#define SMGW_LMN_INTERFACE_H

#include "serial.h"
// include order matters on windows...
#include "config.h"
#include "cosem.h"
#include "hdlc.h"
#include "obis.h"
#include "tls.h"

namespace smgw
{

class lmn_interface
{
public:
	lmn_interface(configuration&& config, meter_profile_map&& meter_profiles,
	              meter_profile_write_func f = nullptr);
	lmn_interface(lmn_interface const&) = delete;
	~lmn_interface();

	cosem::object cosem_request(device_id const& meter_id, obis::code obis_code,
	                            cosem::id cosem_id);

	vec<device_id> connected_meters() const;

	// crypto init / sym
	void crypto_init(device_id const& meter_id);
	bool crypto_reset(device_id const& meter_id);

private:
	void delete_sessions(device_id const& meter_id);

	configuration config_;
	meter_profile_store meter_profiles_;
	serial::serial_port serial_;
	hdlc::lmn::master hdlc_master_;
	tls::manager tls_manager_;
	std::mutex request_mutex;
};

} // namespace smgw

#endif // SMGW_LMN_INTERFACE_H
