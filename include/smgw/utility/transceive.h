#ifndef SMGW_TRANSCEIVE_H
#define SMGW_TRANSCEIVE_H

#include "utility.h"
#include <functional>
#include <optional>

namespace smgw
{

using transceive_func =
    std::function<std::optional<vec<u8>>(const device_id&, std::optional<vec<u8>>&&)>;

}

#endif // SMGW_TRANSCEIVE_H
