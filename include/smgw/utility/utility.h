#ifndef SMGW_UTILITY_H
#define SMGW_UTILITY_H

#include <array>
#include <cassert>
#include <cstdint>
#include <type_traits>
#include <vector>

namespace smgw
{

template <typename T>
using vec = std::vector<T>;

using u8  = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using device_id = std::array<u8, 10>;

template <typename T, T Min, T Max>
class bounded
{
	static_assert(Min <= Max, "Min must be less than or equal to Max");

public:
	constexpr bounded(T value = Min) noexcept
	    : value_((value > Max) ? Max : ((value < Min) ? Min : value))
	{}

	constexpr operator T() const noexcept { return value_; }

	// ++var
	constexpr bounded& operator++() noexcept
	{
		value_ = (value_ >= Max) ? Min : (value_ + 1);
		return *this;
	}

	// var++
	constexpr bounded operator++(int) const noexcept
	{
		auto copy = *this;
		return ++copy;
	}

	// --var
	constexpr bounded& operator--() noexcept
	{
		value_ = (value_ <= Min) ? Max : (value_ - 1);
		return *this;
	}

	// var--
	constexpr bounded operator--(int) const noexcept
	{
		auto copy = *this;
		return --copy;
	}

private:
	T value_;
};

template <typename Enum, typename = std::enable_if_t<std::is_enum_v<Enum>>>
constexpr auto enum_val(Enum e) noexcept
{
	return static_cast<std::underlying_type_t<Enum>>(e);
}

// helper function for sets and maps
template <typename AssociativeContainer, typename Key>
constexpr bool contains(const AssociativeContainer& ac, const Key& k)
{
	return ac.find(k) != ac.end();
}

// std::visit helper
template <class... Ts>
struct overloaded : Ts...
{
	using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...)->overloaded<Ts...>;

} // namespace smgw

#endif // SMGW_UTILITY_H
