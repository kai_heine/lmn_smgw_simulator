#ifndef SMGW_SYM_H
#define SMGW_SYM_H

#include "smgw/meter_profile.h"
#include "smgw/utility/transceive.h"
#include "smgw/utility/utility.h"
#include <optional>

namespace smgw::sym
{

constexpr std::size_t block_size = 16;

using key = std::array<u8, block_size>;

struct key_pair
{
	key enc;
	key mac;
};

enum class message_direction
{
	meter_to_gateway,
	gateway_to_meter
};

struct message
{
	u8 command;
	u32 transmission_counter;
	std::optional<vec<u8>> plaintext_data;
};

std::array<u8, block_size> calculate_mac(key const& mac_key, vec<u8> const& message_buf);
key_pair derive_keys(key const& master_key, message_direction direction, u32 transmission_counter,
                     device_id const& meter_id);
vec<u8> encrypt_data(key const& enc_key, vec<u8> const& plaintext);
vec<u8> decrypt_data(key const& enc_key, vec<u8> const& ciphertext);
vec<u8> serialize_message(message const& msg, message_direction direction, key const& master_key,
                          device_id const& meter_id);
message deserialize_message(vec<u8> const& msg_buf, message_direction direction,
                            key const& master_key, device_id const& meter_id);

[[nodiscard]] meter_profile exchange_tls_credentials(const device_id& meter_id,
                                                     meter_profile&& profile,
                                                     transceive_func transceive);

struct cert_key
{
	vec<u8> certificate;
	vec<u8> private_key;
};

enum class iana_cipher_suite : u16
{
	TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 = 0xc023,
	TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384 = 0xc024,
	TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 = 0xc02b,
	TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 = 0xc02c
};

enum class iana_named_curve : u16
{
	secp256r1       = 23,
	secp384r1       = 24,
	brainpoolP256r1 = 26,
	brainpoolP384r1 = 27,
	brainpoolP512r1 = 28
};

struct supported_algorithms
{
	vec<iana_cipher_suite> cipher_suites;
	vec<iana_named_curve> curve_names;
};

supported_algorithms parse_supported_algorithms(vec<u8> const& payload);

cert_key generate_cert_and_key(const std::string& common_name, iana_named_curve ecc_curve);
cert_key get_gateway_cert_key();

} // namespace smgw::sym

#endif // SMGW_SYM_H
