#include "smgw/sym.h"
#include "smgw/error.h"
#include "smgw/log.h"
#include "smgw/meter_profile.h"
#include "smgw/utility/utility.h"
#include "utility/openssl_helper.h"
#include <filesystem>
#include <fstream>
#include <memory>
#include <openssl/cmac.h>
#include <openssl/ec.h>
#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#include <optional>
#include <spdlog/fmt/bin_to_hex.h>
namespace fs = std::filesystem;

namespace smgw::sym
{

constexpr u8 response_cmd_mask = 0x80;

template <typename Integer, typename OutputIterator>
constexpr OutputIterator write_big_endian(Integer value, OutputIterator out)
{
	static_assert(std::is_integral_v<Integer>, "Value is not an integer");

	for (std::size_t i = 0; i < sizeof(Integer); ++i) {
		*out++ = (value >> ((sizeof(Integer) - 1 - i) * CHAR_BIT)) & 0xff;
	}

	return out;
}

template <typename Integer, typename InputIterator>
constexpr Integer read_big_endian(InputIterator begin, InputIterator end)
{
	Integer value{};
	for (std::size_t i = 0; (i < sizeof(Integer)) && (begin != end); ++i) {
		value <<= CHAR_BIT;
		value |= *begin++;
	}
	return value;
}

supported_algorithms parse_supported_algorithms(vec<u8> const& payload)
{
	assert(payload.size() >= 10);

	auto sym_version = read_big_endian<u32>(begin(payload), end(payload));
	if (sym_version != 0xffff0001) {
		// TODO error
	}
	auto it = begin(payload) + sizeof(u32);

	supported_algorithms result;

	std::size_t num_cipher_suites = *it++;
	for (std::size_t i = 0; i < num_cipher_suites; ++i) {
		result.cipher_suites.push_back(iana_cipher_suite{read_big_endian<u16>(it, end(payload))});
		it += sizeof(u16);
	}

	std::size_t num_curve_names = *it++;
	for (std::size_t i = 0; i < num_curve_names; ++i) {
		result.curve_names.push_back(iana_named_curve{read_big_endian<u16>(it, end(payload))});
		it += sizeof(u16);
	}

	return result;
}

meter_profile exchange_tls_credentials(const device_id& meter_id, meter_profile&& profile,
                                       transceive_func transceive)
{
	auto log = spdlog::get("sym");

	// TODO: handle key not present (optional? precondition?)
	key master_key;
	std::copy(begin(profile.sensor_key), end(profile.sensor_key), begin(master_key));

	supported_algorithms meter_algorithms;

	u32 transmission_counter = 0xffffffff;
	for (u8 command = 0x01; command <= 0x05; ++command) {
		std::optional<vec<u8>> request_payload;

		switch (command) {
		case 0x03: {
			// meter cert + key
			request_payload = vec<u8>{};

			auto const key_pair = generate_cert_and_key("meter", meter_algorithms.curve_names[0]);
			request_payload->reserve(key_pair.certificate.size() + key_pair.private_key.size());
			request_payload->insert(end(*request_payload), begin(key_pair.certificate),
			                        end(key_pair.certificate));
			request_payload->insert(end(*request_payload), begin(key_pair.private_key),
			                        end(key_pair.private_key));
			break;
		}
		case 0x04: {
			// gateway cert
			request_payload = vec<u8>{};

			auto const gateway_key_pair = get_gateway_cert_key();
			request_payload->reserve(gateway_key_pair.certificate.size());
			request_payload->insert(end(*request_payload), begin(gateway_key_pair.certificate),
			                        end(gateway_key_pair.certificate));
			break;
		}
		default: break;
		}

		auto request = message{command, transmission_counter, request_payload};
		auto response_buf =
		    transceive(meter_id, serialize_message(request, message_direction::gateway_to_meter,
		                                           master_key, meter_id));

		while (!response_buf) {
			response_buf = transceive(meter_id, std::nullopt);
		}

		auto response = deserialize_message(*response_buf, message_direction::meter_to_gateway,
		                                    master_key, meter_id);

		if (response.plaintext_data.has_value()) {
			log->debug("response plaintext:\n{}", spdlog::to_hex(*response.plaintext_data));
		}

		if (response.command != (command | response_cmd_mask)) {
			// TODO: discard connection
			throw std::system_error(sym_errc::unexpected_frame, "Invalid SYM command code");
		}

		transmission_counter = response.transmission_counter + 1;

		switch (response.command) {
		case 0x82: {
			// sym version (0xFFFF0001), cipher suites, curves
			meter_algorithms = parse_supported_algorithms(*response.plaintext_data);
			break;
		}
		case 0x85: {
			// max fragment length
			auto max_fragment_length = read_big_endian<u16>(begin(*response.plaintext_data),
			                                                end(*response.plaintext_data));
			log->info("Supported maximum fragment length: {}", max_fragment_length);
			profile.max_fragment_length = max_fragment_length;
			break;
		}
		default: break;
		}
	}

	return std::move(profile);
}

constexpr u8 padding(std::size_t length)
{
	return 16 - (length % 16);
}

struct crypto_deleter
{
	void operator()(CMAC_CTX* ctx) { CMAC_CTX_free(ctx); }
	void operator()(EVP_CIPHER_CTX* ctx) { EVP_CIPHER_CTX_free(ctx); }
	void operator()(EVP_PKEY* pkey) { EVP_PKEY_free(pkey); }
	void operator()(X509* x509) { X509_free(x509); }
	void operator()(BIO* bio) { BIO_free(bio); }
	void operator()(X509_EXTENSION* ex) { X509_EXTENSION_free(ex); }
};
using cmac_ctx_ptr       = std::unique_ptr<CMAC_CTX, crypto_deleter>;
using evp_cipher_ctx_ptr = std::unique_ptr<EVP_CIPHER_CTX, crypto_deleter>;
using evp_pkey_ptr       = std::unique_ptr<EVP_PKEY, crypto_deleter>;
using x509_ptr           = std::unique_ptr<X509, crypto_deleter>;
using bio_ptr            = std::unique_ptr<BIO, crypto_deleter>;
using x509_extension_ptr = std::unique_ptr<X509_EXTENSION, crypto_deleter>;

std::array<u8, block_size> calculate_mac(key const& mac_key, vec<u8> const& message_buf)
{
	auto ctx = cmac_ctx_ptr{CMAC_CTX_new()};
	if (!ctx) {
		utility::handle_openssl_error();
	}

	utility::openssl_return_code ret;
	ret = CMAC_Init(ctx.get(), mac_key.data(), mac_key.size(), EVP_aes_128_cbc(), nullptr);
	ret = CMAC_Update(ctx.get(), message_buf.data(), message_buf.size());
	std::size_t out_len = 0;
	std::array<u8, block_size> result;
	ret = CMAC_Final(ctx.get(), result.data(), &out_len);

	return result;
}

key_pair derive_keys(key const& master_key, message_direction direction, u32 transmission_counter,
                     device_id const& meter_id)
{
	using namespace utility;

	/* meter to gateway:
	 * k_enc = 0x00, k_mac = 0x01
	 * gateway to meter:
	 * l_enc = 0x10, l_mac = 0x11
	 */
	constexpr u8 enc_mask = 0x00;
	constexpr u8 mac_mask = 0x01;
	u8 key_identifier     = (direction == message_direction::meter_to_gateway) ? 0x00 : 0x10;

	vec<u8> key_material;
	key_material.reserve(block_size);

	key_material.push_back(key_identifier);
	write_big_endian(transmission_counter, std::back_inserter(key_material));
	std::copy(begin(meter_id), end(meter_id), std::back_inserter(key_material));
	auto pad = padding(key_material.size());
	key_material.insert(end(key_material), pad, pad);

	key_pair result;

	key_material[0] = key_identifier | enc_mask;
	result.enc      = calculate_mac(master_key, key_material);
	key_material[0] = key_identifier | mac_mask;
	result.mac      = calculate_mac(master_key, key_material);

	return result;
}

vec<u8> encrypt_data(key const& enc_key, vec<u8> const& plaintext)
{
	auto ctx = evp_cipher_ctx_ptr{EVP_CIPHER_CTX_new()};
	if (!ctx) {
		utility::handle_openssl_error();
	}

	utility::openssl_return_code ret;

	constexpr std::array<u8, block_size> iv{};
	ret = EVP_EncryptInit_ex(ctx.get(), EVP_aes_128_cbc(), nullptr, enc_key.data(), iv.data());

	auto pad_len = padding(plaintext.size());
	vec<u8> ciphertext(plaintext.size() + pad_len);

	int len                    = 0;
	std::size_t ciphertext_len = 0;
	ret = EVP_EncryptUpdate(ctx.get(), ciphertext.data(), &len, plaintext.data(), plaintext.size());
	ciphertext_len = len;

	ret = EVP_EncryptFinal_ex(ctx.get(), ciphertext.data() + len, &len);
	ciphertext_len += len;
	ciphertext.resize(ciphertext_len);

	return ciphertext;
}

vec<u8> decrypt_data(key const& enc_key, vec<u8> const& ciphertext)
{
	auto ctx = evp_cipher_ctx_ptr{EVP_CIPHER_CTX_new()};
	if (!ctx) {
		utility::handle_openssl_error();
	}

	utility::openssl_return_code ret;
	constexpr std::array<u8, block_size> iv{};
	ret = EVP_DecryptInit_ex(ctx.get(), EVP_aes_128_cbc(), nullptr, enc_key.data(), iv.data());

	vec<u8> plaintext(ciphertext.size());
	int len                   = 0;
	std::size_t plaintext_len = 0;
	ret =
	    EVP_DecryptUpdate(ctx.get(), plaintext.data(), &len, ciphertext.data(), ciphertext.size());
	plaintext_len = len;

	ret = EVP_DecryptFinal_ex(ctx.get(), plaintext.data() + len, &len);
	plaintext_len += len;
	plaintext.resize(plaintext_len);

	return plaintext;
}

vec<u8> serialize_message(message const& msg, message_direction direction, key const& master_key,
                          device_id const& meter_id)
{
	auto keys = derive_keys(master_key, direction, msg.transmission_counter, meter_id);

	vec<u8> ciphertext;
	if (msg.plaintext_data.has_value()) {
		ciphertext = encrypt_data(keys.enc, *msg.plaintext_data);
	}

	u32 message_length = 1 + 4 + 4 + ciphertext.size() + 16; // command, length, tcnt, data, mac

	vec<u8> buffer;
	buffer.reserve(message_length);
	buffer.push_back(msg.command);
	write_big_endian(message_length, std::back_inserter(buffer));
	write_big_endian(msg.transmission_counter, std::back_inserter(buffer));
	std::copy(begin(ciphertext), end(ciphertext), std::back_inserter(buffer));

	auto mac = calculate_mac(keys.mac, buffer);
	std::copy(begin(mac), end(mac), std::back_inserter(buffer));

	return buffer;
}

message deserialize_message(vec<u8> const& msg_buf, message_direction direction,
                            key const& master_key, device_id const& meter_id)
{
	assert(msg_buf.size() >= (1 + 4 + 4 + 16));
	message result;
	result.command              = msg_buf[0];
	result.transmission_counter = read_big_endian<u32>(begin(msg_buf) + 5, end(msg_buf));

	auto keys = derive_keys(master_key, direction, result.transmission_counter, meter_id);

	if (msg_buf.size() > (1 + 4 + 4 + 16)) {
		result.plaintext_data =
		    decrypt_data(keys.enc, vec<u8>(begin(msg_buf) + (1 + 4 + 4), end(msg_buf) - 16));
	}

	auto mac = calculate_mac(keys.mac, vec<u8>(begin(msg_buf), end(msg_buf) - 16));
	if (!std::equal(begin(mac), end(mac), end(msg_buf) - 16)) {
		throw std::system_error(sym_errc::invalid_mac);
	}

	return result;
}

unsigned char const* uchar(std::string const& str)
{
	return reinterpret_cast<unsigned char const*>(str.c_str());
}

unsigned char const* operator"" _u(char const* str, std::size_t)
{
	return reinterpret_cast<unsigned char const*>(str);
}

constexpr int iana_curve_to_nid(iana_named_curve curve)
{
	switch (curve) {
	case iana_named_curve::secp256r1: return NID_X9_62_prime256v1;
	case iana_named_curve::secp384r1: return NID_secp384r1;
	case iana_named_curve::brainpoolP256r1: return NID_brainpoolP256r1;
	case iana_named_curve::brainpoolP384r1: return NID_brainpoolP384r1;
	case iana_named_curve::brainpoolP512r1: return NID_brainpoolP512r1;
	default: return NID_X9_62_prime256v1;
	}
}

cert_key generate_cert_and_key(const std::string& common_name, iana_named_curve ecc_curve)
{
	utility::openssl_return_code ret;

	// private key
	auto pkey = evp_pkey_ptr{EVP_PKEY_new()};
	// gets freed by EVP_PKEY_free
	auto ec_key = EC_KEY_new_by_curve_name(iana_curve_to_nid(ecc_curve));
	ret         = EC_KEY_generate_key(ec_key);
	ret         = EVP_PKEY_assign_EC_KEY(pkey.get(), ec_key);

	// certificate
	auto x509 = x509_ptr{X509_new()};

	ret = X509_set_version(x509.get(), 0x2); // version 3
	ret = ASN1_INTEGER_set(X509_get_serialNumber(x509.get()), 1);

	// usage time
	X509_gmtime_adj(X509_getm_notBefore(x509.get()), 0);
	X509_gmtime_adj(X509_getm_notAfter(x509.get()), 7 * 365 * 24 * 60 * 60); // seven years

	// public key
	ret = X509_set_pubkey(x509.get(), pkey.get());

	// subject and issuer names
	auto name = X509_get_subject_name(x509.get());
	ret       = X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, "DE"_u, -1, -1, 0);
	ret       = X509_NAME_add_entry_by_txt(name, "O", MBSTRING_ASC, "ESG"_u, -1, -1, 0);
	ret       = X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, uchar(common_name), -1, -1, 0);
	ret       = X509_set_issuer_name(x509.get(), name);

	// x509v3 extensions
	X509V3_CTX x509v3_ctx;
	X509V3_set_ctx(&x509v3_ctx, x509.get(), x509.get(), nullptr, nullptr, 0);
	auto ext = x509_extension_ptr{};
	// basic constraints
	ext.reset(X509V3_EXT_conf_nid(nullptr, &x509v3_ctx, NID_basic_constraints, "CA:FALSE"));
	ret = X509_add_ext(x509.get(), ext.get(), -1);
	// subject key identifier
	ext.reset(X509V3_EXT_conf_nid(nullptr, &x509v3_ctx, NID_subject_key_identifier, "hash"));
	ret = X509_add_ext(x509.get(), ext.get(), -1);
	// key usage
	ext.reset(X509V3_EXT_conf_nid(nullptr, &x509v3_ctx, NID_key_usage,
	                              "digitalSignature,keyEncipherment"));
	ret = X509_add_ext(x509.get(), ext.get(), -1);

	// self-sign certificate
	ret = X509_sign(x509.get(), pkey.get(), EVP_sha256());

	// write to buffer in DER encoding
	cert_key result;
	auto bio = bio_ptr{BIO_new(BIO_s_mem())};

	ret = i2d_PrivateKey_bio(bio.get(), pkey.get());
	result.private_key.resize(BIO_ctrl_pending(bio.get()));
	BIO_read(bio.get(), result.private_key.data(), result.private_key.size());

	ret = i2d_X509_bio(bio.get(), x509.get());
	result.certificate.resize(BIO_ctrl_pending(bio.get()));
	BIO_read(bio.get(), result.certificate.data(), result.certificate.size());

	return result;
}

const fs::path gateway_certificate = "gateway_certificate.cer";
const fs::path gateway_private_key = "gateway_private_key.key";

vec<u8> read_file(const fs::path& file_path)
{
	std::ifstream file(file_path.string(), std::ios::binary | std::ios::ate);
	std::size_t file_len = file.tellg();
	file.seekg(0);
	vec<u8> buffer(file_len);
	file.read(reinterpret_cast<char*>(buffer.data()), file_len);
	file.close();

	return buffer;
}

void write_file(const fs::path& file_path, vec<u8> const& buffer)
{
	std::ofstream file(file_path.string(), std::ios::binary);
	file.write(reinterpret_cast<char const*>(buffer.data()), buffer.size());
	file.close();
}

cert_key get_gateway_cert_key()
{
	cert_key result;

	if (fs::exists(gateway_certificate) && fs::exists(gateway_private_key)) {
		result.certificate = read_file(gateway_certificate);
		result.private_key = read_file(gateway_private_key);
	}
	else {
		result = generate_cert_and_key("smgw", iana_named_curve::secp256r1);
		write_file(gateway_certificate, result.certificate);
		write_file(gateway_private_key, result.private_key);
	}

	return result;
}

} // namespace smgw::sym
