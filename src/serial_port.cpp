#include "smgw/serial/serial_port.h"
#include "smgw/error.h"
#include "smgw/hdlc/frame.h"
// log.h must be included after serial_port.h because of boost::asio issues with winsock
#include "smgw/log.h"
#include <algorithm>
#include <boost/asio/read.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/write.hpp>
#include <mutex>

namespace smgw::serial
{
using boost::asio::dynamic_buffer;
using boost::asio::steady_timer;
using namespace std::chrono;

namespace
{
void flush(boost::asio::serial_port& port)
{
	auto handle = port.native_handle();
#ifdef _WIN32
	if (PurgeComm(handle, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR) == FALSE) {
		throw std::system_error(std::error_code(GetLastError(), std::system_category()));
	}
#else
	if (tcflush(handle, TCIOFLUSH) == -1) {
		throw std::system_error(std::error_code(errno, std::system_category()));
	}
#endif
}
} // namespace

// precondition: none
// postcondition: supplied port could be opened
serial_port::serial_port(const std::string& device)
{
	using namespace boost;
	try {
		port.open(device);
		port.set_option(asio::serial_port::baud_rate(921600));
		port.set_option(asio::serial_port::character_size(8));
		port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
		port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
		port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
	}
	catch (const boost::system::system_error& e) {
		throw std::system_error{e.code(), std::string("Could not open serial port ") + device};
	}
	flush(port);
	fmt::print("Successfully opened serial port {}\n", device);
}

// precondition: none
// postcondition: a valid frame was read before the timeout
vec<u8> serial_port::read_single_frame(milliseconds timeout)
{
	auto logger = spdlog::get("serial port");
	using namespace std::chrono_literals;
	if (timeout <= 0ms) {
		logger->warn("Receive timeout is 0 or less. This is probably not what you want.");
	}

	std::lock_guard<std::mutex> lock(serial_mutex);

	using boost::asio::async_read;
	vec<u8> buffer;
	buffer.reserve(3);

	io_ctx.restart();
	steady_timer timer(io_ctx);

	auto frame_length_handler = [&timer, &buffer, this](auto error, auto) {
		if (!error) {
			if (buffer[0] != hdlc::constants::flag) {
				throw std::system_error(serial_errc::invalid_frame);
			}

			const auto frame_length = hdlc::frame_length(buffer);

			buffer.reserve(frame_length);

			async_read(port, dynamic_buffer(buffer, frame_length), [&timer](auto err, auto) {
				if (!err) {
					timer.cancel();
				}
			});
		}
	};

	async_read(port, dynamic_buffer(buffer, 3), frame_length_handler);

	timer.expires_after(timeout);
	timer.async_wait([this](auto error) {
		if (!error) {
			// cancel read when timer expires
			port.cancel();
			throw std::system_error(serial_errc::timeout);
		}
	});

	// blocks until either read or timer are finished
	auto start = steady_clock::now();
	io_ctx.run();
	auto end = steady_clock::now();

	logger->trace("time: {}ms", duration_cast<milliseconds>(end - start).count());

	logger->debug("Received {} bytes:\n{}", buffer.size(), log_helper{buffer});

	return buffer;
}

// precondition: none
// postcondition: only valid frames or none at all are returned
vec<vec<u8>> serial_port::read_for(std::chrono::milliseconds duration)
{
	auto logger = spdlog::get("serial port");
	using namespace std::chrono_literals;
	if (duration <= 0ms) {
		logger->warn("Receive duration is 0 or less. This is probably not what you want.");
	}

	using boost::asio::async_read;
	vec<u8> buffer;

	{ // only lock while receiving
		std::lock_guard<std::mutex> lock(serial_mutex);

		steady_timer timer(io_ctx);
		io_ctx.restart();

		async_read(port, dynamic_buffer(buffer),
		           // no completion condition: just read everything that comes in
		           // read handler, called when read is finished
		           [&timer](auto error, auto) {
			           if (!error) {
				           timer.cancel();
			           }
		           });

		timer.expires_after(duration);
		timer.async_wait([this](auto error) {
			if (!error) {
				port.cancel();
			}
		});

		io_ctx.run();
	}

	if (buffer.empty()) {
		logger->debug("Received nothing.");
	}
	else {
		logger->debug("Received {} bytes:\n{}", buffer.size(), log_helper{buffer});
	}

	return hdlc::split_frames(buffer);
}

// precondition: none
// postcondition: writing was successfull
void serial_port::write(vec<u8>&& frame)
{
	std::lock_guard<std::mutex> lock(serial_mutex);

	spdlog::get("serial port")->debug("Sending {} bytes:\n{}", frame.size(), log_helper{frame});

	boost::asio::write(port, boost::asio::buffer(frame));
}

} // namespace smgw::serial
