#include "smgw/lmn_interface.h"
#include "smgw/error.h"
#include "smgw/log.h"
#include "smgw/sml.h"
#include "smgw/sym.h"
#include <cmath>
#include <thread>

namespace smgw
{

template <typename Callable, typename... Args>
void spawn(Callable callable, Args... args)
{
	std::thread{callable, args...}.detach();
}

lmn_interface::lmn_interface(smgw::configuration&& config, meter_profile_map&& meter_profiles,
                             meter_profile_write_func f)
    : config_{std::move(config)},
      meter_profiles_{std::move(meter_profiles), std::move(f)},
      serial_{config_.serial_configuration.port_name},
      hdlc_master_{serial_},
      tls_manager_{meter_profiles_}
{
	smgw::log::init(config_.log_configuration);

	// start cyclic broadcasts in a different thread
	spawn([this]() {
		using namespace std::chrono_literals;
		while (true) {
			std::chrono::steady_clock::time_point now;
			{
				std::lock_guard<std::mutex> lock(request_mutex);
				now = std::chrono::steady_clock::now();
				hdlc_master_.do_broadcast();
			}
			std::this_thread::sleep_until(now + 15s);
		}
	});
}

lmn_interface::~lmn_interface()
{
	spdlog::drop_all();
}

smgw::cosem::object lmn_interface::cosem_request(smgw::device_id const& meter_id,
                                                 smgw::obis::code obis_code,
                                                 smgw::cosem::id cosem_id)
{
	using namespace smgw;
	using smgw::hdlc::lmn::protocol_selector;

	auto obis_array = [&]() {
		std::array<std::uint8_t, 6> arr{};
		for (std::size_t i = 0; i < arr.size(); ++i) {
			arr[i] = (enum_val(obis_code) >> ((arr.size() - 1 - i) * CHAR_BIT)) & 0xff;
		}
		return arr;
	}();

	try {
		std::lock_guard<std::mutex> lock(request_mutex);

		// use tls if meter profile exists
		auto profile = meter_profiles_.get(meter_id);
		if (profile) {
			return sml::read_object(
			    meter_id, obis_array, cosem_id,
			    tls::transceiver{tls_manager_.get_session(meter_id),
			                     hdlc::lmn::unicast_transceiver<protocol_selector::tls_sml_cosem>{
			                         hdlc_master_}});
		}
		else {
			return sml::read_object(
			    meter_id, obis_array, cosem_id,
			    hdlc::lmn::unicast_transceiver<protocol_selector::sml_cosem>{hdlc_master_});
		}
	}
	catch (std::system_error const& e) {
		if (e.code().category() != get_sml_category()) {
			delete_sessions(meter_id);
		}
		throw;
	}
}

vec<device_id> lmn_interface::connected_meters() const
{
	auto slaves = hdlc_master_.connected_slaves();
	vec<device_id> ret;
	ret.reserve(slaves.size());

	for (auto const& slave : slaves) {
		ret.push_back(slave.first);
	}
	return ret;
}

void lmn_interface::crypto_init(device_id const& meter_id)
{
	using namespace smgw;
	using smgw::hdlc::lmn::protocol_selector;

	auto profile = meter_profiles_.get(meter_id);
	if (!profile) {
		throw std::runtime_error("no meter profile present");
		// TODO error code
	}

	try {
		std::lock_guard<std::mutex> lock(request_mutex);

		auto new_profile = sym::exchange_tls_credentials(
		    meter_id, std::move(profile.value()),
		    hdlc::lmn::unicast_transceiver<protocol_selector::sym>{hdlc_master_});

		meter_profiles_.update(meter_id, std::move(new_profile));
	}
	catch (std::system_error const& e) {
		if (e.code().category() != get_sml_category()) {
			delete_sessions(meter_id);
		}
		throw;
	}
}

bool lmn_interface::crypto_reset(const device_id& meter_id)
{
	using smgw::hdlc::lmn::protocol_selector;

	try {
		std::lock_guard<std::mutex> lock(request_mutex);

		sml::write_object(
		    meter_id, hdlc::lmn::unicast_transceiver<protocol_selector::sml_cosem>(hdlc_master_));
	}
	catch (std::system_error const& e) {
		if (e.code().category() != get_sml_category()) {
			delete_sessions(meter_id);
		}
		throw;
	}
	return true;
}

void lmn_interface::delete_sessions(const device_id& meter_id)
{
	tls_manager_.delete_session(meter_id);
	hdlc_master_.delete_session(meter_id);
}

} // namespace smgw
