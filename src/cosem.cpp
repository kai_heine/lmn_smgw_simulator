#include "smgw/cosem.h"
#include "smgw/utility/utility.h"
#include <string>

namespace smgw::cosem{

std::string to_string(unit_type u)
{
	switch (u) {
	case unit_type::watt_hour: return "Wh";
	case unit_type::watt: return "W";
	case unit_type::ampere: return "A";
	case unit_type::volt: return "V";
	default: break;
	}
	return "(" + std::to_string(smgw::enum_val(u)) + ")";
}

}
