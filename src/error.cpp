#include "smgw/error.h"

namespace smgw
{

//
// general errors
//
namespace
{
struct general_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* general_category::name() const noexcept
{
	return "general";
}

std::string general_category::message(int ev) const
{
	switch (static_cast<general_errc>(ev)) {
	case general_errc::openssl_error: return "OpenSSL error";
	default: return "unknown error";
	}
}

const general_category general_category_object{};

} // namespace

std::error_code make_error_code(general_errc e)
{
	return {static_cast<int>(e), general_category_object};
}

//
// serial port errors
//
namespace
{
struct serial_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* serial_category::name() const noexcept
{
	return "serial port";
}

std::string serial_category::message(int ev) const
{
	switch (static_cast<serial_errc>(ev)) {
	case serial_errc::timeout: return "timeout";
	case serial_errc::invalid_frame: return "invalid frame";
	default: return "unknown error";
	}
}

const serial_category serial_category_object{};

} // namespace

std::error_code make_error_code(serial_errc e)
{
	return {static_cast<int>(e), serial_category_object};
}

//
// hdlc errors
//
namespace
{
struct hdlc_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* hdlc_category::name() const noexcept
{
	return "HDLC";
}

std::string hdlc_category::message(int ev) const
{
	switch (static_cast<hdlc_errc>(ev)) {
	case hdlc_errc::invalid_control_field_format: return "invalid control field format";
	case hdlc_errc::invalid_checksum: return "invalid checksum";
	case hdlc_errc::invalid_address: return "invalid address";
	case hdlc_errc::slave_not_connected: return "slave not connected";
	case hdlc_errc::unexpected_response_frame: return "unexpected response frame";
	default: return "unknown error";
	}
}

const hdlc_category hdlc_category_object{};

} // namespace

std::error_code make_error_code(hdlc_errc e)
{
	return {static_cast<int>(e), hdlc_category_object};
}

//
// tls errors
//
namespace
{
struct tls_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* tls_category::name() const noexcept
{
	return "TLS";
}

std::string tls_category::message(int ev) const
{
	switch (static_cast<tls_errc>(ev)) {
	case tls_errc::missing_certs_and_keys: return "missing certificates and keys";
	default: return "unknown error";
	}
}

const tls_category tls_category_object{};

} // namespace

std::error_code make_error_code(tls_errc e)
{
	return {static_cast<int>(e), tls_category_object};
}

//
// sml errors
//
namespace
{
struct sml_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* sml_category::name() const noexcept
{
	return "SML";
}

std::string sml_category::message(int ev) const
{
	switch (static_cast<sml_errc>(ev)) {
	case sml_errc::differing_object_id: return "differing object id";
	case sml_errc::object_attributes_missing: return "object attributes missing";
	case sml_errc::unsupported_obis_number: return "unsupported obis number";
	case sml_errc::unsupported_cosem_class: return "unsupported cosem class";
	case sml_errc::invalid_response: return "invalid response";

	case sml_errc::unspecified_failure: return "unspecified failure";
	case sml_errc::unknown_sml_identifier: return "unknown sml designator";
	case sml_errc::attributes_not_writable: return "attributes not writable";
	case sml_errc::attributes_not_readable: return "attributes not readable";
	case sml_errc::value_supplied_outside_permissable_range:
		return "value supplied is outside the permissible value range";
	case sml_errc::request_not_executed: return "request not executed";
	case sml_errc::temporarily_unavailable: return "temporarily unavailable";
	// TODO: add attention messages
	default: return "unknown error";
	}
}

const sml_category sml_category_object{};

} // namespace

const std::error_category& get_sml_category() noexcept
{
	return sml_category_object;
}

std::error_code make_error_code(sml_errc e)
{
	return {static_cast<int>(e), sml_category_object};
}

//
// sym errors
//
namespace
{
struct sym_category : std::error_category
{
	[[nodiscard]] const char* name() const noexcept override;
	[[nodiscard]] std::string message(int ev) const override;
};

const char* sym_category::name() const noexcept
{
	return "SYM";
}

std::string sym_category::message(int ev) const
{
	switch (static_cast<sym_errc>(ev)) {
	case sym_errc::invalid_mac: return "invalid mac";
	case sym_errc::unexpected_frame: return "unexpected frame";
	default: return "unknown error";
	}
}

const sym_category sym_category_object{};

} // namespace

std::error_code make_error_code(sym_errc e)
{
	return {static_cast<int>(e), sym_category_object};
}

} // namespace smgw
