#ifndef SMGW_UTILITY_OPENSSL_HELPER_H
#define SMGW_UTILITY_OPENSSL_HELPER_H

namespace smgw::utility
{

void handle_openssl_error();

struct openssl_return_code
{
	openssl_return_code(int e = 1);

	constexpr operator int() const { return ret; }

private:
	int ret;
};

} // namespace smgw::utility

#endif // SMGW_UTILITY_OPENSSL_HELPER_H
