#include "openssl_helper.h"
#include "smgw/error.h"
#include <openssl/err.h>

namespace smgw::utility
{

void handle_openssl_error()
{
	const auto error_str = ERR_error_string(ERR_get_error(), nullptr);
	throw std::system_error(general_errc::openssl_error, error_str);
}

openssl_return_code::openssl_return_code(int e) : ret{e}
{
	if (e < 1) {
		handle_openssl_error();
	}
	ERR_clear_error();
}

} // namespace smgw::utility
