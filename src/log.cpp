#include "smgw/log.h"
#include "smgw/utility/utility.h"
#include <spdlog/async.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <string_view>

namespace smgw
{

namespace log
{

constexpr std::string_view indent = "    ";

void init(const configuration& cfg)
{
	// make sure this is only ever done once
	static bool initialized = false;
	if (initialized) {
		return;
	}
	initialized = true;

	// TODO: add file sink

	spdlog::stdout_color_mt<spdlog::async_factory>("smgw");
	spdlog::stdout_color_mt<spdlog::async_factory>("serial port");
	spdlog::stdout_color_mt<spdlog::async_factory>("hdlc");
	spdlog::stdout_color_mt<spdlog::async_factory>("tls");
	spdlog::stdout_color_mt<spdlog::async_factory>("sml");
	spdlog::stdout_color_mt<spdlog::async_factory>("sym");

	spdlog::set_level(cfg.log_level);
	spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%^%l%$] [%n] %v");
}

} // namespace log

std::ostream& operator<<(std::ostream& os, log_helper<vec<u8>> raw_bytes)
{
	std::size_t i = 0;
	os << log::indent << fmt::format("{:04X}: ", i);
	for (; i < raw_bytes.data.size(); i++) {
		os << fmt::format("{:02X} ", raw_bytes.data[i]);
		if ((i + 1) % 16 == 0) {
			os << '\n' << log::indent << fmt::format("{:04X}: ", i + 1);
		}
	}

	return os;
}

namespace hdlc
{
std::ostream& operator<<(std::ostream& os, const frame& hdlc_frame)
{
	auto address_str = [](lmn::address addr) -> std::string {
		switch (addr) {
		case lmn::address::smgw: return "SMGw";
		case lmn::address::broadcast: return "Broadcast";
		default: return fmt::format("0x{:02X}", enum_val(addr));
		}
	};

	auto destination = address_str(hdlc_frame.destination.dynamic_address);
	auto source      = address_str(hdlc_frame.source.dynamic_address);

	auto ctrl = std::visit([](const auto& f) { return f.name; }, hdlc_frame.control);

	auto protocol = [&hdlc_frame]() -> std::string {
		using lmn::protocol_selector;
		if (std::holds_alternative<frame_function::ui>(hdlc_frame.control)) {
			switch (hdlc_frame.destination.session_protocol) {
			case protocol_selector::query_new: return "query new";
			case protocol_selector::check_missing: return "check missing";
			default: break;
			}
		}
		else {
			switch (hdlc_frame.destination.session_protocol) {
			case protocol_selector::tls_sml_cosem: return "TLS/SML/COSEM";
			case protocol_selector::sml_cosem: return "SML/COSEM";
			case protocol_selector::sym: return "SYM";
			default: break;
			}
		}
		return fmt::format("0x{:02X}", enum_val(hdlc_frame.destination.session_protocol));
	}();

	auto payload = [](const auto& hdlc_payload) -> std::string {
		if (hdlc_payload.empty()) {
			return "no payload.";
		}
		return fmt::format("{} byte payload.", hdlc_payload.size());
	}(hdlc_frame.payload);

	os << fmt::format("{} -> {}, {} frame, {}, {}", source, destination, ctrl, protocol, payload);

	return os;
}

namespace lmn
{

std::ostream& operator<<(std::ostream& os, log_helper<slave_map> known_slaves)
{
	for (const auto& [meter_id, info] : known_slaves.data) {
		os << '\n'
		   << log::indent
		   << fmt::format("Address: [0x{:02X}], ID: [", enum_val(info.slave_address));
		for (const auto& b : meter_id) {
			os << fmt::format("{:02X}", b);
		}
		os << "]";
	}

	return os;
}

} // namespace lmn
} // namespace hdlc
} // namespace smgw
