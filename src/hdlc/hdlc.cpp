#include "smgw/hdlc.h"
#include "crc.h"
#include "smgw/error.h"
#include "smgw/log.h"

namespace smgw::hdlc
{

// precondition: none
// postcondition: valid control_field
control_field deserialize_control_field(u8 ctrl)
{
	using namespace frame_function;

	if ((ctrl & 0x01) == i::mask) {
		return i{ctrl};
	}

	switch (ctrl & 0b0000'1111) {
	case rr::mask: return rr{ctrl};
	case rnr::mask: return rnr{ctrl};
	default: break;
	}

	switch (ctrl & 0b11101111) {
	case ui::mask: return ui{};
	case dm::mask: return dm{};
	case ua::mask: return ua{};
	case disc::mask: return disc{};
	case snrm::mask: return snrm{};
	case frmr::mask: return frmr{};
	default: throw std::system_error(hdlc_errc::invalid_control_field_format);
	}
}

// precondition: none
// postcondition: dynamic address != 0
address deserialize_address(u8 msb, u8 lsb)
{
	address a{static_cast<lmn::address>(msb >> 1), static_cast<lmn::protocol_selector>(lsb >> 1)};

	if (a.dynamic_address == lmn::address::forbidden) {
		throw std::system_error(hdlc_errc::invalid_address);
	}
	// don't care about a valid protocol selector, must be checked by the application anyway

	return a;
}

// precondition: frame starts and ends with a flag sequence, is at least 11 bytes long
// postcondition: all fields could be deserialized, valid crc
frame deserialize_frame(const vec<u8>& frame_bytes)
{
	assert(frame_bytes.size() >= 11);
	assert(frame_bytes.front() == constants::flag && frame_bytes.back() == constants::flag);

	frame f{deserialize_address(frame_bytes[frame_index::dst_adr_msb],
	                            frame_bytes[frame_index::dst_adr_lsb]),
	        deserialize_address(frame_bytes[frame_index::src_adr_msb],
	                            frame_bytes[frame_index::src_adr_lsb]),
	        deserialize_control_field(frame_bytes[frame_index::ctrl_field]),
	        {}};

	if (frame_bytes.size() > 11) // payload exists
	{
		auto header_checking_sequence = fcs::checking_sequence(
		    frame_bytes.begin() + 1, frame_bytes.begin() + frame_index::hdr_crc_msb);

		if (header_checking_sequence[0] != frame_bytes[frame_index::hdr_crc_msb] ||
		    header_checking_sequence[1] != frame_bytes[frame_index::hdr_crc_lsb]) {
			throw std::system_error(hdlc_errc::invalid_checksum);
		}

		f.payload.assign(frame_bytes.begin() + 10, frame_bytes.end() - 3);
	}

	auto frame_checking_sequence =
	    fcs::checking_sequence(frame_bytes.begin() + 1, frame_bytes.end() - 3);

	auto fcs_index = frame_bytes.size() - 3;
	if (frame_checking_sequence[0] != frame_bytes[fcs_index] ||
	    frame_checking_sequence[1] != frame_bytes[fcs_index + 1]) {
		throw std::system_error(hdlc_errc::invalid_checksum);
	}

	return f;
}

// precondition: none
// postcondition: byte representation
std::array<u8, 2> address::serialize() const noexcept
{
	u8 msb = enum_val(dynamic_address) << 1;
	u8 lsb = (enum_val(session_protocol) << 1) | 0x01;
	return {msb, lsb};
}

namespace frame_function
{

constexpr u8 poll_final = 0b00010000; // poll/final bit is always 1 [LMN_0193][LMN_0194]

template <typename FrameFunction>
u8 serialize(const FrameFunction& ff)
{
	return ff.mask | poll_final;
}

template <>
u8 serialize<i>(const i& ff)
{
	return i::mask | (ff.receive_sequence_number << 5) | poll_final |
	       (ff.send_sequence_number << 1);
}

template <>
u8 serialize<rr>(const rr& ff)
{
	return rr::mask | poll_final | (ff.receive_sequence_number << 5);
}

template <>
u8 serialize<rnr>(const rnr& ff)
{
	return rnr::mask | poll_final | (ff.receive_sequence_number << 5);
}

} // namespace frame_function

// precondition: frame is valid
// postcondition: byte representation
vec<u8> frame::serialize() const
{
	std::size_t length = payload.empty() ? 11 : payload.size() + 13;
	vec<u8> bytes(length);

	bytes.front() = constants::flag;

	length -= 2; // the hdlc length field does not count the start and end flags
	bytes[frame_index::frame_format_msb] =
	    constants::frame_format_type_3 | static_cast<u8>(length >> 8);
	bytes[frame_index::frame_format_lsb] = length & 0xff;

	auto destination_bytes          = destination.serialize();
	bytes[frame_index::dst_adr_msb] = destination_bytes[0];
	bytes[frame_index::dst_adr_lsb] = destination_bytes[1];

	auto source_bytes               = source.serialize();
	bytes[frame_index::src_adr_msb] = source_bytes[0];
	bytes[frame_index::src_adr_lsb] = source_bytes[1];

	bytes[frame_index::ctrl_field] =
	    visit([](const auto& c) { return frame_function::serialize(c); }, control);

	auto header_checking_sequence =
	    fcs::checking_sequence(bytes.begin() + 1, bytes.begin() + frame_index::hdr_crc_msb);
	bytes[frame_index::hdr_crc_msb] = header_checking_sequence[0];
	bytes[frame_index::hdr_crc_lsb] = header_checking_sequence[1];

	if (!payload.empty()) {
		std::copy(payload.begin(), payload.end(), bytes.begin() + frame_index::payload);

		auto frame_checking_sequence = fcs::checking_sequence(bytes.begin() + 1, bytes.end() - 3);
		auto fcs_index               = bytes.size() - 3;
		bytes[fcs_index]             = frame_checking_sequence[0];
		bytes[fcs_index + 1]         = frame_checking_sequence[1];
	}
	bytes.back() = constants::flag;

	return bytes;
}

// precondition: none
// postcondition: all extracted frames begin and end with flag sequence, have valid length
vec<vec<u8>> split_frames(const vec<u8>& buffer)
{
	vec<vec<u8>> frames;

	auto frame_begin = std::find(buffer.begin(), buffer.end(), constants::flag);

	while (frame_begin != buffer.end()) {
		if (std::distance(frame_begin, buffer.end()) < 3) {
			break;
		}
		const auto length    = frame_length(frame_begin, buffer.end());
		const auto frame_end = frame_begin + length;

		if ((frame_end > buffer.end()) || (*(frame_end - 1) != constants::flag)) {
			// stop at first invalid frame
			break;
		}

		frames.emplace_back(frame_begin, frame_end);
		frame_begin = std::find(frame_end, buffer.end(), constants::flag);
	}

	return frames;
}
} // namespace smgw::hdlc
