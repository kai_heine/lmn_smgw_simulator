#include "smgw/hdlc/lmn/master.h"
#include "smgw/error.h"
#include "smgw/hdlc/lmn/utility.h"
#include "smgw/log.h"
#include <list>
#include <set>
#include <thread>

namespace smgw::hdlc::lmn
{

// precondition: none
// postcondition: byte representation
vec<u8> serialize_broadcast_payload(const slave_map& connected_slaves, bool assign_time_slots)
{
	vec<u8> broadcast_payload;
	broadcast_payload.reserve(connected_slaves.size() * 32);
	u8 time_slot{0};

	for (const auto& [device_id, info] : connected_slaves) {
		vec<u8> slave_payload(32, 0); // all bytes not written are zero-filled

		slave_payload[0] = enum_val(info.slave_address);
		slave_payload[1] = assign_time_slots ? ++time_slot : 0;
		std::copy(device_id.begin(), device_id.end(), slave_payload.begin() + 2);
		std::copy(info.sensor_id.begin(), info.sensor_id.end(), slave_payload.begin() + 16);
		slave_payload[30] = enum_val(info.state) >> 8;
		slave_payload[31] = enum_val(info.state) & 0xff;

		broadcast_payload.insert(broadcast_payload.end(), slave_payload.begin(),
		                         slave_payload.end());
	}
	return broadcast_payload;
}

// precondition: payload has right size
// postcondition: meter info could be deserialized
slave_map::value_type deserialize_broadcast_payload(const vec<u8>& broadcast_payload)
{
	assert(broadcast_payload.size() == 32);

	device_id meter_id{};
	std::copy_n(broadcast_payload.begin() + 2, meter_id.size(), meter_id.begin());

	slave_info info;
	info.slave_address = static_cast<lmn::address>(broadcast_payload[0]);
	std::copy_n(broadcast_payload.begin() + 16, info.sensor_id.size(), info.sensor_id.begin());
	info.state =
	    static_cast<slave_info::cardinality>((broadcast_payload[30] << 8) | broadcast_payload[31]);

	return {meter_id, info};
}

// precondition: none
// postcondition: broadcast was performed
void master::do_broadcast()
{
	using std::chrono::duration_cast;
	using std::chrono::milliseconds;
	using namespace std::chrono_literals;

	auto log = spdlog::get("hdlc");

	auto broadcast_frame = make_ui_frame(
	    next_broadcast_state_,
	    serialize_broadcast_payload(connected_slaves_,
	                                (next_broadcast_state_ == protocol_selector::check_missing)));

	{
		std::unique_lock<std::shared_mutex> writer_lock(broadcast_mutex);

		// TODO: make transceive wrapper function
		log->debug("Sending broadcast frame.");
		log->debug("{}", broadcast_frame);
		serial.write(broadcast_frame.serialize());

		constexpr int max_meters          = 63;         // [LMN_0058]
		constexpr auto guard_time         = 5ms * 1.05; // [LMN_0045]
		constexpr auto time_slot_duration = 5ms * 1.05; // [LMN_0046]

		constexpr auto response_time =
		    duration_cast<milliseconds>(max_meters * (guard_time + time_slot_duration));

		auto received_frames = serial.read_for(response_time);

		connected_slaves_ =
		    update_slave_map(std::move(connected_slaves_), next_broadcast_state_, received_frames);

		if (!connected_slaves_.empty()) {
			log->info("Known meters after broadcast:{}", log_helper{connected_slaves_});
		}

		next_broadcast_state_ = next_broadcast_state_ == protocol_selector::query_new ?
		                            protocol_selector::check_missing :
		                            protocol_selector::query_new;
	}
	// TODO: return some info? number of meters connected?
}

// preconditions: none
// postconditions: map contains updated list of connected slaves
slave_map update_slave_map(slave_map&& connected_slaves, protocol_selector broadcast_state,
                           const vec<vec<u8>>& broadcast_responses)
{
	auto log = spdlog::get("hdlc");

	vec<std::pair<device_id, slave_info>> response_infos;
	response_infos.reserve(broadcast_responses.size());

	for (const auto& frame_bytes : broadcast_responses) {
		try {
			auto frame_info = deserialize_frame(frame_bytes);
			log->debug("Broadcast response: {}", frame_info);

			if (frame_info.destination.dynamic_address != address::smgw ||
			    frame_info.destination.session_protocol != broadcast_state ||
			    !std::holds_alternative<frame_function::ui>(frame_info.control) ||
			    frame_info.payload.size() != 32) {
				continue;
			}

			const auto [meter_id, info] = deserialize_broadcast_payload(frame_info.payload);
			response_infos.emplace_back(meter_id, info);
		}
		catch (const std::system_error& e) {
			if (e.code() == hdlc_errc::invalid_checksum) {
				log->warn("Broadcast response has invalid checksum. Ignoring.");
			}
			else {
				log->warn("{}", e.what());
			}
		}
	}

	if (broadcast_state == protocol_selector::query_new) {
		// remove duplicates from responses
		std::set<address> addresses;
		std::set<address> to_be_deleted;
		for (const auto& [meter_id, info] : response_infos) {
			const auto unique = addresses.insert(info.slave_address).second;
			if (!unique) {
				to_be_deleted.insert(info.slave_address);
			}
		}

		if (!to_be_deleted.empty()) {
			log->warn("Multiple slaves chose the same address. Ignoring all.");
		}

		// remove all entries with addresses already assigned
		for (const auto& [meter_id, info] : connected_slaves) {
			to_be_deleted.insert(info.slave_address);
		}

		// delete entries
		for (const auto addr : to_be_deleted) {
			response_infos.erase(std::remove_if(response_infos.begin(), response_infos.end(),
			                                    [addr](const auto& pair) {
				                                    return pair.second.slave_address == addr;
			                                    }),
			                     response_infos.end());
		}
		// insert new elements into map
		for (const auto& [meter_id, info] : response_infos) {
			connected_slaves.try_emplace(meter_id, info);
		}
	}
	else {
		// delete all entries not present in new list
		auto it = connected_slaves.begin();
		while (it != connected_slaves.end()) {
			if (std::find_if(response_infos.begin(), response_infos.end(), [it](const auto& pair) {
				    return pair.second.slave_address == it->second.slave_address;
			    }) == response_infos.end()) {
				connected_slaves.erase(it++);
			}
			else {
				++it;
			}
		}
	}

	return std::move(connected_slaves);
}

bool valid_addresses(const frame& response_frame, address slave_address) noexcept
{
	return !((response_frame.destination.dynamic_address != address::smgw) ||
	         (response_frame.source.dynamic_address != slave_address));
}

bool valid_protocol(const frame& response_frame, const session& active_session) noexcept
{
	return !((response_frame.destination.session_protocol != active_session.protocol) ||
	         (response_frame.source.session_protocol != active_session.protocol));
}

bool valid_sequence_numbers(const hdlc::control_field& control, const session& active_session)
{
	using namespace hdlc::frame_function;

	return std::visit(
	    overloaded{
	        [&](const i& arg) {
		        return (arg.receive_sequence_number == active_session.send_sequence_variable) &&
		               (arg.send_sequence_number == active_session.receive_sequence_variable);
	        },
	        [&](const rnr& arg) {
		        return arg.receive_sequence_number == active_session.send_sequence_variable;
	        },
	        [&](const rr& arg) {
		        return arg.receive_sequence_number == active_session.send_sequence_variable;
	        },
	        [](const auto&) { return true; }},
	    control);
}

// preconditions: none
// postconditions: the connection could be or is already established, other connections are closed
session establish_connection(address destination, protocol_selector protocol,
                             std::optional<session> active_session, transceive_func transceive)
{
	// ISO 13239
	// SNRM -> UA (ack) or DM (reject new connection)
	// DISC -> UA (ack) or DM (already disconnected)

	auto log = spdlog::get("hdlc");

	// discard current session, if any
	if (active_session && active_session->protocol != protocol) {
		auto disc_frame = make_disc_frame(destination, active_session->protocol);
		log->debug("Closing active session: {}", disc_frame);
		auto response_frame = deserialize_frame(transceive(disc_frame.serialize()));
		log->debug("Slave response: {}", response_frame);
		if ((!std::holds_alternative<frame_function::ua>(response_frame.control) &&
		     !std::holds_alternative<frame_function::dm>(response_frame.control)) ||
		    !valid_addresses(response_frame, destination) ||
		    !valid_protocol(response_frame, *active_session)) {
			throw std::system_error(hdlc_errc::unexpected_response_frame);
		}
		active_session.reset();
	}

	// establish new connection
	if (!active_session) {
		auto snrm_frame = make_snrm_frame(destination, protocol);
		log->debug("Establishing new session: {}", snrm_frame);
		auto response_frame = deserialize_frame(transceive(snrm_frame.serialize()));
		log->debug("Slave response: {}", response_frame);
		if (!std::holds_alternative<frame_function::ua>(response_frame.control) ||
		    !valid_addresses(response_frame, destination)) {
			throw std::system_error(hdlc_errc::unexpected_response_frame);
		}
		return {protocol, 0, 0};
	}

	return *active_session;
}

// preconditions: none
// postconditions: the message has been sent and a response was received
// return value: if empty optional, the other side needs more data
std::optional<vec<u8>> unicast(address destination, session& active_session,
                               std::optional<vec<u8>>&& payload, transceive_func transceive)
{
	// ISO 13239
	// I -> I (data available) or RR (need more data) or RNR (busy)

	auto log = spdlog::get("hdlc");

	frame request_frame;

	bool sending_i_frame = payload.has_value();
	if (sending_i_frame) {
		request_frame = make_i_frame(
		    destination, active_session.protocol, active_session.send_sequence_variable,
		    active_session.receive_sequence_variable, std::forward<vec<u8>>(*payload));
	}
	else {
		request_frame = make_rr_frame(destination, active_session.protocol,
		                              active_session.receive_sequence_variable);
	}

	log->debug("Sending request: {}", request_frame);
	auto response_frame = deserialize_frame(transceive(request_frame.serialize()));
	log->debug("Slave response: {}", response_frame);

	if (sending_i_frame) {
		// increment ssn only after transmitting i frame
		++active_session.send_sequence_variable;
	}

	if (!valid_addresses(response_frame, destination) ||
	    !valid_protocol(response_frame, active_session) ||
	    !valid_sequence_numbers(response_frame.control, active_session)) {
		throw std::system_error(hdlc_errc::unexpected_response_frame);
	}

	while (std::holds_alternative<frame_function::rnr>(response_frame.control)) {
		auto rr_frame = make_rr_frame(destination, active_session.protocol,
		                              active_session.receive_sequence_variable);
		// TODO: polling interval?
		log->debug("Polling for information frame: {}", rr_frame);
		response_frame = deserialize_frame(transceive(rr_frame.serialize()));
		log->debug("Slave response: {}", response_frame);

		if (!valid_addresses(response_frame, destination) ||
		    !valid_protocol(response_frame, active_session) ||
		    !valid_sequence_numbers(response_frame.control, active_session)) {
			throw std::system_error(hdlc_errc::unexpected_response_frame);
		}
	}

	if (std::holds_alternative<frame_function::rr>(response_frame.control)) {
		// RR frame: slave needs another I frame
		return std::nullopt;
	}
	else if (!std::holds_alternative<frame_function::i>(response_frame.control)) {
		throw std::system_error(hdlc_errc::unexpected_response_frame);
	}

	// increment rsn after receiving i frame
	++active_session.receive_sequence_variable;

	return response_frame.payload;
}

// preconditions: none
// postconditions: the request could be fullfilled and the response payload is returned
// return value: empty optional means the other side needs more data
std::optional<vec<u8>> master::do_unicast(const device_id& meter_id,
                                          std::optional<vec<u8>>&& payload,
                                          protocol_selector protocol)
{
	auto log = spdlog::get("hdlc");

	auto transceive = [this](vec<u8>&& frame) {
		using namespace std::chrono_literals;
		std::this_thread::sleep_for(100ms);
		serial.write(std::forward<vec<u8>>(frame));
		return serial.read_single_frame(2s); // TODO: make configurable
	};

	if (!contains(connected_slaves_, meter_id)) {
		throw std::system_error(hdlc_errc::slave_not_connected);
	}

	auto& slave_info = connected_slaves_.at(meter_id);

	slave_info.active_session = establish_connection(slave_info.slave_address, protocol,
	                                                 slave_info.active_session, transceive);

	return unicast(slave_info.slave_address, *slave_info.active_session,
	               std::forward<decltype(payload)>(payload), transceive);
}

slave_map master::connected_slaves() const noexcept
{
	std::shared_lock<std::shared_mutex> reader_lock(broadcast_mutex);
	return connected_slaves_;
}

void master::delete_session(const device_id& meter_id)
{
	if (contains(connected_slaves_, meter_id)) {
		connected_slaves_.at(meter_id).active_session.reset();
	}
}

protocol_selector master::next_broadcast_state() const noexcept
{
	std::shared_lock<std::shared_mutex> reader_lock(broadcast_mutex);
	return next_broadcast_state_;
}

} // namespace smgw::hdlc::lmn
