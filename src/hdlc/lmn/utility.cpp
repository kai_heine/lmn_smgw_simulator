#include "smgw/hdlc/lmn/utility.h"

namespace smgw::hdlc::lmn
{

frame make_snrm_frame(address destination, protocol_selector protocol)
{
	return frame{hdlc::address{destination, protocol},
	             hdlc::address{address::smgw, protocol},
	             frame_function::snrm{},
	             {}};
}

frame make_i_frame(address destination, protocol_selector protocol,
                   frame_function::sequence_number ssn, frame_function::sequence_number rsn,
                   vec<u8>&& payload)
{
	return frame{hdlc::address{destination, protocol}, hdlc::address{address::smgw, protocol},
	             frame_function::i{rsn, ssn}, payload};
}

frame make_ui_frame(protocol_selector protocol, vec<u8>&& payload)
{
	return frame{hdlc::address{address::broadcast, protocol},
	             hdlc::address{address::smgw, protocol}, frame_function::ui{}, payload};
}

frame make_disc_frame(address destination, protocol_selector protocol)
{
	return frame{hdlc::address{destination, protocol},
	             hdlc::address{address::smgw, protocol},
	             frame_function::disc{},
	             {}};
}

frame make_rr_frame(address destination, protocol_selector protocol,
                    frame_function::sequence_number rsn)
{
	return frame{hdlc::address{destination, protocol},
	             hdlc::address{address::smgw, protocol},
	             frame_function::rr{rsn},
	             {}};
}

frame make_rnr_frame(address destination, protocol_selector protocol,
                     frame_function::sequence_number rsn)
{
	return frame{hdlc::address{destination, protocol},
	             hdlc::address{address::smgw, protocol},
	             frame_function::rnr{rsn},
	             {}};
}
} // namespace smgw::hdlc::lmn
