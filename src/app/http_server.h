#ifndef SMGW_HTTP_SERVER_H
#define SMGW_HTTP_SERVER_H

#include "request_handler.h"
#include <boost/asio/ip/tcp.hpp>

class http_server
{
public:
	http_server(std::string_view address, std::uint16_t port, request_handler&& req_handler)
	    : acceptor{io_ctx, {boost::asio::ip::make_address(address), port}},
	      handler{std::move(req_handler)}
	{}

	void serve();

private:
	boost::asio::io_context io_ctx{};
	boost::asio::ip::tcp::acceptor acceptor;
	request_handler handler;
};

#endif // SMGW_HTTP_SERVER_H
