#include "http_server.h"
#include <boost/beast.hpp>
#include <nlohmann/json.hpp>

namespace ip   = boost::asio::ip;
namespace http = boost::beast::http;
using nlohmann::json;

void http_server::serve()
{
	while (true) {
		auto socket = ip::tcp::socket{io_ctx};
		acceptor.accept(socket);

		auto buffer = boost::beast::flat_buffer{};
		bool session_complete{false};

		while (!session_complete) {
			auto req = http::request<boost::beast::http::string_body>{};
			try {
				http::read(socket, buffer, req);
				auto const res = handler(std::move(req));

				auto sr = http::response_serializer<http::string_body>{res};
				http::write(socket, sr);
				if (res.need_eof()) {
					session_complete = true;
				}
			}
			catch (boost::system::system_error const& error) {
				if (error.code() == http::error::end_of_stream) {
					session_complete = true;
				}
				else {
					throw;
				}
			}
		}
		socket.shutdown(ip::tcp::socket::shutdown_send);
	}
}
