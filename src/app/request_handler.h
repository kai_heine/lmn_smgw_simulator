#ifndef SMGW_REQUEST_HANDLER_H
#define SMGW_REQUEST_HANDLER_H

#ifdef _MSC_VER
#	include <sdkddkver.h>
#endif
#include <boost/beast/http.hpp>
// include order matters on windows...
#include <smgw/lmn_interface.h>

class request_handler
{
public:
	explicit request_handler(smgw::lmn_interface& lmn_if) : lmn{lmn_if} {}

	auto operator()(boost::beast::http::request<boost::beast::http::string_body>&& req) const
	    -> boost::beast::http::response<boost::beast::http::string_body>;

private:
	smgw::lmn_interface& lmn;
};

#endif // SMGW_REQUEST_HANDLER_H
