#include "smgw/meter_profile.h"
#include <algorithm>
#include <charconv>
#include <spdlog/fmt/fmt.h>

namespace smgw
{

meter_profile_store::meter_profile_store(meter_profile_map&& meter_profiles,
                                         meter_profile_write_func f)
    : meter_profiles_{std::move(meter_profiles)}, write_meter_profiles_{std::move(f)}
{}

std::optional<meter_profile> meter_profile_store::get(const device_id& meter_id) const
{
	try {
		return meter_profiles_.at(meter_id);
	}
	catch (std::out_of_range const&) {
		return {};
	}
}

void meter_profile_store::update(const device_id& meter_id, meter_profile&& profile)
{
	meter_profiles_[meter_id] = profile;
	if (write_meter_profiles_) {
		write_meter_profiles_(meter_profiles_);
	}
}

device_id device_id_str_to_hex(std::string device_id_str)
{
	std::transform(begin(device_id_str), end(device_id_str), begin(device_id_str),
	               [](char c) { return std::toupper(c); });
	auto medium = static_cast<u8>(device_id_str[0] - '0');

	auto fabrication_block = u8{0};
	std::from_chars(&device_id_str[4], &device_id_str[6], fabrication_block, 16);

	auto fabrication_number = u32{0};
	std::from_chars(&device_id_str[6], &device_id_str[14], fabrication_number, 10);

	return device_id{0x0A,
	                 medium,
	                 static_cast<u8>(device_id_str[1]),
	                 static_cast<u8>(device_id_str[2]),
	                 static_cast<u8>(device_id_str[3]),
	                 fabrication_block,
	                 static_cast<u8>(fabrication_number >> 24),
	                 static_cast<u8>(fabrication_number >> 16),
	                 static_cast<u8>(fabrication_number >> 8),
	                 static_cast<u8>(fabrication_number >> 0)};
}

std::string device_id_hex_to_str(device_id const& id)
{
	auto medium             = id[1];
	auto manufacturer_id    = std::string{id.begin() + 2, id.begin() + 5};
	auto fabrication_block  = id[5];
	auto fabrication_number = (id[6] << 24) | (id[7] << 16) | (id[8] << 8) | (id[9] << 0);

	return fmt::format("{:1d}{:s}{:02X}{:08d}", medium, manufacturer_id, fabrication_block,
	                   fabrication_number);
}

} // namespace smgw
