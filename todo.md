# TODO

- make separate apis for unencrypted and tls encrypted requests
- make timings configurable
- proper cosem write functionality
    - write arbitrary cosem objects
- full pairing
    1. sym exchange
    2. change master key over tls
- use templates instead of std::function?
