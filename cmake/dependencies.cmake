cmake_minimum_required(VERSION 3.14)


# System-wide dependencies

find_package(Threads REQUIRED)

if(SMGW_STATIC_BUILD)
	set(OPENSSL_USE_STATIC_LIBS ON CACHE BOOL "" FORCE)
	if(MSVC)
		set(OPENSSL_MSVC_STATIC_RT ON CACHE BOOL "" FORCE)
	endif()
endif()
find_package(OpenSSL 1.1.1 REQUIRED)

set(Boost_USE_STATIC_LIBS ON CACHE BOOL "" FORCE)
find_package(Boost 1.67 REQUIRED
	COMPONENTS 
		system
		regex 
		date_time
)


# Automatically downloaded dependencies

include(FetchContent)

FetchContent_Declare(smlpp
	GIT_REPOSITORY https://gitlab.com/kai_heine/smlpp.git
)
FetchContent_MakeAvailable(smlpp)



FetchContent_Declare(nlohmann_json
    GIT_REPOSITORY https://github.com/nlohmann/json.git
    GIT_TAG v3.10.1
)
set(JSON_BuildTests OFF CACHE BOOL "" FORCE)
set(JSON_Install OFF CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(nlohmann_json)



FetchContent_Declare(spdlog
	GIT_REPOSITORY https://github.com/gabime/spdlog.git
	GIT_TAG v1.9.2
)
FetchContent_MakeAvailable(spdlog)
# mark as SYSTEM library to silence warnings
get_target_property(SPDLOG_INCLUDES spdlog INTERFACE_INCLUDE_DIRECTORIES)
set_target_properties(spdlog
    PROPERTIES INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "${SPDLOG_INCLUDES}"
)



FetchContent_Declare(args
	GIT_REPOSITORY https://github.com/Taywee/args.git
	GIT_TAG 6.2.6
)
set(ARGS_BUILD_EXAMPLE OFF CACHE BOOL "" FORCE)
set(ARGS_BUILD_UNITTESTS OFF CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(args)



FetchContent_Declare(yaml-cpp
	GIT_REPOSITORY https://github.com/jbeder/yaml-cpp.git
	GIT_TAG yaml-cpp-0.7.0
)
set(YAML_CPP_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(YAML_CPP_BUILD_TOOLS OFF CACHE BOOL "" FORCE)
set(YAML_CPP_BUILD_CONTRIB OFF CACHE BOOL "" FORCE)
set(YAML_CPP_INSTALL OFF CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(yaml-cpp)
# mark as SYSTEM library to silence warnings
get_target_property(YAML_CPP_INCLUDES yaml-cpp INTERFACE_INCLUDE_DIRECTORIES)
set_target_properties(yaml-cpp
    PROPERTIES INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "${YAML_CPP_INCLUDES}"
)


# Test dependencies

FetchContent_Declare(doctest
	GIT_REPOSITORY https://github.com/onqtam/doctest.git
	GIT_TAG 2.4.6
)
set(DOCTEST_WITH_TESTS OFF CACHE BOOL "" FORCE)
set(DOCTEST_WITH_MAIN_IN_STATIC_LIB OFF CACHE BOOL "" FORCE)
set(DOCTEST_NO_INSTALL ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(doctest)


FetchContent_Declare(trompeloeil
	GIT_REPOSITORY https://github.com/rollbear/trompeloeil.git
	GIT_TAG v41
)
FetchContent_MakeAvailable(trompeloeil)

