# LMN SMGw Simulator

C++ implementation of the wired LMN communication stack of a Smart Meter Gateway. Work in progress.

## Building

Build requirements:
- C++17 compiler
- CMake >=3.14
- Boost >=1.67
- OpenSSL 1.1.1

Run CMake to build the project:

```shell
mkdir build && cd build
cmake ..
cmake --build .
```

## Running

Run `lmn_smgw_simulator` from the `bin` directory.

Currently, a REST API with JSON payload is provided at `localhost:8080`:

URL | Description
--- | ---
`/connected_meters` | List connected meters.
`/cosem/meter/<meter_id>/object/<class_id>-<object>/` | Read meter values identified by their COSEM class id and OBIS number.
`/crypto_init/meter/<meter_id>/` | Perform certificate exchange with meter (SYM handshake).
`/crypto_reset/meter/<meter_id>/` | Reset cryptographic information of meter.

Example: `/cosem/meter/1ABC0012345678/object/32770-0100010800FF`

### TLS

In order to read meters via TLS, certificates and keys must be exchanged first.

1. Add your meter id and master key to `meters.yml`.
2. Run `lmn_smgw_simulator` and invoke the SYM handshake.
3. Read meter values. If the SYM handshake was successful, TLS is automatically used.

### Options

See `config.yml` or run `lmn_smgw_simulator --help` for options.

