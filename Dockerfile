# build native image by default
# change with --build-arg ARCH=armhf
ARG ARCH=native

#native base
FROM alpine:3.9 as native

# armhf base
FROM arm32v7/alpine:3.9 as armhf
ADD ./qemu-arm-static.tar.gz /usr/bin

# build stage
FROM ${ARCH} as builder
RUN apk add --no-cache g++ ninja cmake openssl-dev boost-dev boost-static git
WORKDIR /home/build
COPY . /home
RUN cmake -G Ninja -DCMAKE_BUILD_TYPE=MinSizeRel ..
RUN cmake --build . --target lmn_smgw_simulator

# final image
FROM ${ARCH}
WORKDIR /smgw/
COPY ./bin .
COPY --from=builder /home/build/lmn_smgw_simulator /usr/bin
RUN apk add --no-cache libstdc++ libssl1.1

ENTRYPOINT ["/usr/bin/lmn_smgw_simulator"]
